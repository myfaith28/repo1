    package com.example.fmpdroid.fragments;

    import android.app.Activity;
    import android.os.AsyncTask;
    import android.os.Bundle;
    import android.support.annotation.Nullable;
    import android.support.v4.app.ListFragment;
    import android.util.Log;
    import android.view.View;
    import android.widget.ArrayAdapter;
    import android.widget.ListView;
    import android.widget.Toast;

    import com.example.fmpdroid.models.ChairmanPOV;

    import org.apache.http.HttpEntity;
    import org.apache.http.HttpResponse;
    import org.apache.http.client.HttpClient;
    import org.apache.http.client.methods.HttpPost;
    import org.apache.http.impl.client.DefaultHttpClient;
    import org.apache.http.util.EntityUtils;
    import org.json.JSONArray;
    import org.json.JSONObject;

    import java.util.ArrayList;
    import java.util.List;

    /**
     * Created by fmpdroid on 12/28/2014.
     */
public class FacultyListFragment extends ListFragment {
        OnHeadlineSelectedListener mCallback;

        String facultyUrl = "http://10.0.3.2/touchims/get_faculty.php";
        String facultyName;
        ArrayList<String> faculty = new ArrayList<>();
        String jsonResult = "";

        public interface OnHeadlineSelectedListener {
            public void onArticleSelected(String name);
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);

            try {
                mCallback = (OnHeadlineSelectedListener) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                        + " must implement OnHeadlineSelectedListener");
            }
        }

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            Log.v("POTANGINA1", "pistiatay");
            MyAsyncTask task = new MyAsyncTask();
            new MyAsyncTask().execute();
        }

        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {
            super.onListItemClick(l, v, position, id);
            //Toast.makeText(getActivity(), getListAdapter().getItem(position).toString(), Toast.LENGTH_LONG).show();
            facultyName = getListAdapter().getItem(position).toString();
            facultyName = facultyName.replace(" ","%20");
            mCallback.onArticleSelected(facultyName);
            Log.v("HEY", facultyName);
        }


        private class MyAsyncTask extends AsyncTask<String, Void, String> {


            @Override
            protected String doInBackground(String... params) {

                String facultyResult="";
                try {
                    HttpClient client = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(facultyUrl);
                    Log.v("POTANGINA1", "bilat");
                    HttpResponse response = client.execute(httpPost);
                    HttpEntity httpEntity = response.getEntity();
                    jsonResult = EntityUtils.toString(httpEntity);
                    JSONArray result = new JSONArray(jsonResult);
                    for (int i = 0; i < result.length(); i++) {
                        JSONObject jsonObject = result.getJSONObject(i);
                        facultyResult = jsonObject.getString("faculty_name");
                        faculty.add(facultyResult);
                        Log.v("POTANGINA", facultyResult);
                        Log.v("POTANGINA1", "pisti");
                    }
                } catch (Exception e) {


                }

                return null ;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_list_item_1, faculty);
                setListAdapter(adapter);


            }
        }



    }

    package com.example.fmpdroid.praticelistviews;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.Toast;

import com.example.fmpdroid.fragments.CalendarViewFragment;
import com.example.fmpdroid.fragments.FacultyAttendanceFragment;
import com.example.fmpdroid.fragments.FacultyListFragment;


    public class MainActivity extends ActionBarActivity implements FacultyListFragment.OnHeadlineSelectedListener{

        String facultyName;

        @Override
        public void onArticleSelected(String name) {
            this.facultyName = name;
            Toast.makeText(getApplicationContext(),"HEY"+facultyName,Toast.LENGTH_LONG).show();
            Bundle args = new Bundle();
            args.putString("facultyName", facultyName);
            FacultyAttendanceFragment facultyAttendanceFragment = new FacultyAttendanceFragment();
            facultyAttendanceFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.detailsContainer,facultyAttendanceFragment)
                    .commit();
        }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.adapter);

        FragmentManager fm = getSupportFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();

        ft.add(R.id.listContainer, new FacultyListFragment());
        //ft.add(R.id.detailsContainer, new FacultyAttendanceFragment());
        ft.commit();

        //calendar
        Button btnCalender = (Button) findViewById(R.id.btnDatePicker);

        btnCalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fm = getSupportFragmentManager();

                CalendarViewFragment calendar = new CalendarViewFragment();
                AlertDialog dialog;
                calendar.show(fm, "calendar");
//                dialog = new AlertDialog.Builder(getApplicationContext())
//                        .setTitle("choose date")
//                        .setView(calendar.getView())
//                        .create();
//                dialog.show();


            }
        });


        }

    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }


package com.example.fmpdroid.fragments;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.fmpdroid.models.ChairmanPOV;
import com.example.fmpdroid.praticelistviews.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.fmpdroid.praticelistviews.R.drawable.cell_shape;


/**
 * Created by fmpdroid on 1/2/2015.
 */
public class FacultyAttendanceFragment extends Fragment{


    String dataUrl = "http://10.0.3.2/touchims/get_data.php";
    String jsonResult;
    String facultyName;
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        facultyName = getArguments().getString("facultyName");
        Log.v("taeni",facultyName);
        new MySecondAsyncTask().execute();
        //testing
//        String[] date = {"December 1", "December 2", "December 3"};
//        String[] time = {"8:00-9:00", "9:00-10:00", "10:00-11:00"};
//        String[] room = {"BCL2", "BCL3", "BCL24"};
//
//
//        TableLayout table = (TableLayout) getActivity().findViewById(R.id.detailsTable);
//
//        TableRow row;
//        TextView t1, t2, t3;
//
//        for(int count = 0; count < date.length; count++) {
//            row = new TableRow(getActivity());
//            t1 = new TextView(getAct  ivity());
//            t2 = new TextView(getActivity());
//            t3 = new TextView(getActivity());
//
//            t1.setText(date[count]);
//            t2.setText(time[count]);
//            t3.setText(room[count]);
//            row.addView(t1);
//            row.addView(t2);
//            row.addView(t3);
//
//            table.addView(row, new TableLayout.LayoutParams(
//                    TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
//
//
//        }
    }

//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        new MySecondAsyncTask().execute();
//    }
    //    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//       return null;
//    }

    private class MySecondAsyncTask extends AsyncTask<String, Void, String> {

        List<ChairmanPOV> list =  new ArrayList<ChairmanPOV>();

        @Override
        protected String doInBackground(String... params) {


            String facultyResult="";
            try {
                Log.v("Im here!","WOW1");
                HttpClient client = new DefaultHttpClient();
                Log.v("Im here!","WOW2");
                //HttpPost httpPost = new HttpPost(dataUrl+"?faculty="+facultyName);
                HttpGet httpGet = new HttpGet(dataUrl+"?faculty="+facultyName);
                Log.v("Im here!","WOW3");
                HttpResponse response = client.execute(httpGet);
                Log.v("Im here!","WOW4x");
                HttpEntity httpEntity = response.getEntity();
                jsonResult = EntityUtils.toString(httpEntity);
                JSONArray result = new JSONArray(jsonResult);
                for (int i = 0; i < result.length(); i++) {
                    JSONObject jsonObject = result.getJSONObject(i);
                    int id = jsonObject.getInt("id");
                    String faculty_name = jsonObject.getString("faculty");
                    String date = jsonObject.getString("date");
                    String day = jsonObject.getString("day");
                    String time = jsonObject.getString("time");
                    String room = jsonObject.getString("room");
                    String remarks = jsonObject.getString("remarks");
                    String chairman_feedback = jsonObject.getString("chairman_feedback");
                    String dean_feedback = jsonObject.getString("dean_feedback");

                    list.add(new ChairmanPOV(id,faculty_name,date,day,
                            time,room,remarks,chairman_feedback,dean_feedback));
                }
            } catch (Exception e) {


            }

            return null ;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            TextView t = new TextView(getActivity());
//            for(int i=0; i<list.size(); i++){
//                int id = list.get(i).getId();
//                String facName = list.get(i).getFaculty();
//                //t.setText(facName);
//                Log.v("mothafucka",facName);

            Log.v("mothafucka","HELLOOO1");

            TableLayout table = (TableLayout) getActivity().findViewById(R.id.detailsTable);
            table.removeAllViews();
            Log.v("mothafucka","HELLOOO");

            TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT, 1f);

            TableRow row, rowInfo;
            TextView t1, t2, t3, t4, t5, t6;
            TextView tt1, tt2, tt3, tt4, tt5, tt6;
            rowInfo = new TableRow(getActivity());
            tt1 = new TextView(getActivity());
            tt2 = new TextView(getActivity());
            tt3 = new TextView(getActivity());
            tt4 = new TextView(getActivity());
            tt5 = new TextView(getActivity());
            tt6 = new TextView(getActivity());

            tt1.setText("Time");
            tt1.setBackgroundResource(cell_shape);
            tt1.setTextColor(Color.parseColor("#FFFFFF"));
            tt1.setPadding(5,5,5,5);
            tt1.setLayoutParams(params);

            tt2.setText();
            tt2.setBackgroundResource(cell_shape);
            tt2.setTextColor(Color.parseColor("#FFFFFF"));
            tt2.setPadding(5,5,5,5);
            tt2.setLayoutParams(params);

            tt3.setText(day);
            tt3.setBackgroundResource(cell_shape);
            tt3.setTextColor(Color.parseColor("#FFFFFF"));
            tt3.setPadding(5,5,5,5);
            tt3.setLayoutParams(params);

            tt4.setText(remarks);
            tt4.setBackgroundResource(cell_shape);
            tt4.setTextColor(Color.parseColor("#FFFFFF"));
            tt4.setPadding(5,5,5,5);
            tt4.setLayoutParams(params);

            tt5.setText(chairman);
            tt5.setBackgroundResource(cell_shape);
            tt5.setTextColor(Color.parseColor("#FFFFFF"));
            tt5.setPadding(5,5,5,5);
            tt5.setLayoutParams(params);

            tt6.setText("Chairman");
            tt6.setBackgroundResource(cell_shape);
            tt6.setTextColor(Color.parseColor("#FFFFFF"));
            tt6.setPadding(5,5,5,5);
            tt6.setLayoutParams(params);



            for(int count = 0; count < list.size(); count++) {
            String facName = list.get(count).getFaculty();
            Log.v("mothafucka",facName);
            String room = list.get(count).getRoom();
            Log.v("mothafucka",room);
            String time = list.get(count).getTime();
            Log.v("mothafucka",time);
            String day = list.get(count).getDay();
            String remarks = list.get(count).getRemarks();
            String chairman = list.get(count).getChairman_feedback();
            String dean = list.get(count).getDean_feedback();

            row = new TableRow(getActivity());
            t1 = new TextView(getActivity());
            t2 = new TextView(getActivity());
            t3 = new TextView(getActivity());
            t4 = new TextView(getActivity());
            t5 = new TextView(getActivity());
            t6 = new TextView(getActivity());




            t1.setText(time);
            t1.setBackgroundResource(cell_shape);
            t1.setTextColor(Color.parseColor("#FFFFFF"));
            t1.setPadding(5,5,5,5);
            t1.setLayoutParams(params);

            t2.setText(room);
            t2.setBackgroundResource(cell_shape);
            t2.setTextColor(Color.parseColor("#FFFFFF"));
            t2.setPadding(5,5,5,5);
            t2.setLayoutParams(params);

            t3.setText(day);
            t3.setBackgroundResource(cell_shape);
            t3.setTextColor(Color.parseColor("#FFFFFF"));
            t3.setPadding(5,5,5,5);
            t3.setLayoutParams(params);

            t4.setText(remarks);
            t4.setBackgroundResource(cell_shape);
            t4.setTextColor(Color.parseColor("#FFFFFF"));
            t4.setPadding(5,5,5,5);
            t4.setLayoutParams(params);

            t5.setText(chairman);
            t5.setBackgroundResource(cell_shape);
            t5.setTextColor(Color.parseColor("#FFFFFF"));
            t5.setPadding(5,5,5,5);
            t5.setLayoutParams(params);

            t6.setText(dean);
            t6.setBackgroundResource(cell_shape);
            t6.setTextColor(Color.parseColor("#FFFFFF"));
            t6.setPadding(5,5,5,5);
            t6.setLayoutParams(params);



            row.addView(t1);
            row.addView(t2);
            row.addView(t3);
            row.addView(t4);
            row.addView(t5);
            row.addView(t6);


            table.addView(row);
            }
        }
    }
}

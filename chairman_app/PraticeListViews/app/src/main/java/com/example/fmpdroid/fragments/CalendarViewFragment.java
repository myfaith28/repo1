package com.example.fmpdroid.fragments;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.Toast;

import com.example.fmpdroid.praticelistviews.R;

/**
 * Created by fmpdroid on 1/5/2015.
 */
public class CalendarViewFragment extends DialogFragment{

    CalendarView calendar;
    AlertDialog dialog;

    public CalendarViewFragment(){
    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.calendar, container, false);
//
//        calendar = (CalendarView)view.findViewById(R.id.calendarView);
//
//        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
//            @Override
//            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
//                String mYear = String.valueOf(year);
//                String mMonth = String.valueOf(month+1);
//                String mDay = String.valueOf(dayOfMonth);
//                Toast.makeText(getActivity(),mMonth+"/"+mDay+"/"+mYear,Toast.LENGTH_LONG).show();
//            }
//        });
//        return view;
//    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.calendar, null);


        calendar = (CalendarView)view.findViewById(R.id.calendarView);

        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                String mYear = String.valueOf(year);
                String mMonth = String.valueOf(month+1);
                if(mMonth.length()==1)
                {
                    mMonth = "0"+mMonth;
                }
                String mDay = String.valueOf(dayOfMonth);
                if(mDay.length()==1)
                {
                    mDay = "0"+mDay;
                }
                Toast.makeText(getActivity(),mMonth+"/"+mDay+"/"+mYear,Toast.LENGTH_LONG).show();
            }
        });

        return new AlertDialog.Builder(getActivity())
                .setTitle("Choose Date")
                .setView(view)
                .create();
    }
}
package com.example.fmpdroid.models;

/**
 * Created by fmpdroid on 1/23/2015.
 */
public class ChairmanPOV {

    int id;
    String faculty;
    String date;
    String day;
    String time;
    String room;
    String remarks;
    String chairman_feedback;
    String dean_feedback;

    public ChairmanPOV(){

    }

    public ChairmanPOV(int id, String faculty, String date,
                       String day, String time, String room, String remarks,
                       String chairman_feedback, String dean_feedback) {
        this.id = id;
        this.faculty = faculty;
        this.date = date;
        this.day = day;
        this.time = time;
        this.room = room;
        this.remarks = remarks;
        this.chairman_feedback = chairman_feedback;
        this.dean_feedback = dean_feedback;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getChairman_feedback() {
        return chairman_feedback;
    }

    public void setChairman_feedback(String chairman_feedback) {
        this.chairman_feedback = chairman_feedback;
    }

    public String getDean_feedback() {
        return dean_feedback;
    }

    public void setDean_feedback(String dean_feedback) {
        this.dean_feedback = dean_feedback;
    }
}

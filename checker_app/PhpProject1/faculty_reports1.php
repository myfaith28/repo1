<?php

//select * from subj_schedule where substr(time,1,6) as time_start, substr(time,9) as end_time

require_once('dbconnect.php');

$fr = new faculty_reports1();


if($_POST){
	
	$offer_code = $_POST['offercode'];
	//echo $_POST['enabled'];
	
}

if(!$fr->checkDate_query($connection,$offer_code)){

	if(isset($_POST['action']) && $_POST['action']=='CHECKMARK'){
		$fr->insertReport($offer_code,$connection,'CHECKMARK');
	}
	if(isset($_POST['action']) && $_POST['action']=='XMARK'){
		$fr->insertReport($offer_code,$connection,'XMARK');
	}
	if(isset($_POST['action']) && $_POST['action']=='SAVE'){
		$fr->hitSave($offer_code,$connection);
	}
}



if(isset($_POST['action']) && $_POST['action']=="enable"){
	if(!$fr->checkDate_query($connection,$offer_code))
		echo "enabled";
	else
		echo "disabled";

}


class faculty_reports1{



	function generateReportID($connection){

		$report_id = rand( 1000000 ,99999999);

		$query="select report_id from touch_faculty_report where report_id='$report_id'";
		$result = mysqli_query($connection,$query);
		$rows=mysqli_num_rows($result);
		if($rows>0)
			$this->generateReportID();

		else
			return $report_id;
		

	}

	function checkingStatus($offer_code,$connection){
		$status=null;
		$query = "select checking_status from touch_faculty_report where offer_code='$offer_code'";
		$result = mysqli_query($connection,$query);
		$rows=mysqli_num_rows($result);
		if($rows){
			
			$results = array();
		    while($line = mysqli_fetch_assoc($result)){
		    	$results[] = $line;
		    }
		    $status = $results[0]['checking_status'];
		}
		else{
			$status = "!";
		}
	    return $status;


	}

	function checkingStatusCount($offer_code,$connection){
		$status=null;
		$query = "select checking_status from touch_faculty_report where offer_code='$offer_code'";
		$result = mysqli_query($connection,$query);
		$rows=mysqli_num_rows($result);
		$results = array();
		while($line = mysqli_fetch_assoc($result)){
		    $results[] = $line;
		}
		
		//echo $rows;

	    return $status;


	}

	function hitSave($offer_code,$connection){
		$checkingStatus = $this->checkingStatus($offer_code,$connection);
		$firstChecked = $this->firstChecked($offer_code,$connection);
		$lastChecked = $this->lastChecked($offer_code,$connection);
		$countPendingFirstChecked = $this->countPendingFirstChecked($offer_code,$connection);
		$firstCheckingStat = $this->firstCheckingStat($offer_code,$connection);

		$query=null;

		if(!$firstChecked && $checkingStatus=='0/2'){
			echo "Please select between two";
		}

		if($firstChecked && $checkingStatus=='0/2' && !$countPendingFirstChecked){
			$query="update touch_faculty_report set checking_status='1/2' where offer_code='$offer_code'";
			echo "half done";
		}
		if($checkingStatus=='1/2' && $lastChecked &&!$countPendingFirstChecked){
			$query="update touch_faculty_report set checking_status='2/2' where offer_code='$offer_code'";
			echo "done";

		}

		if($firstCheckingStat=='ABSENT'){
			$query="update touch_faculty_report set checking_status='2/2' , last_checking='ABSENT' where offer_code='$offer_code'";
			echo "done";
		}





		if($query!=NULL){
			$result = mysqli_query($connection,$query) or die("query error");
		}

		else
			echo "save error";
		

	}

	function firstChecked($offer_code,$connection){
		$query="select * from touch_faculty_report where offer_code='$offer_code' and last_checking is NULL";
		$result = mysqli_query($connection,$query);
		$rows=mysqli_num_rows($result);
		if($rows>0)
			return true;
		else
			return false;

	}

	function firstCheckingStat($offer_code,$connection){
		$query="select * from touch_faculty_report where offer_code='$offer_code'";
		$result = mysqli_query($connection,$query);
		$status="";
		$rows=mysqli_num_rows($result);
		if($rows>0){
			$results = array();
		    while($line = mysqli_fetch_assoc($result)){
		    	$results[] = $line;
		    }
		    $status = $results[0]['first_checking'];
		}
	    return $status;
	}



	function lastChecked($offer_code,$connection){
		$query="select * from touch_faculty_report where offer_code='$offer_code' and last_checking is not NULL";
		$result = mysqli_query($connection,$query);
		$rows=mysqli_num_rows($result);
		if($rows>0)
			return true;
		else
			return false;

	}

	function countPendingFirstChecked($offer_code,$connection){
		$query = "select * from touch_faculty_report where first_checking='PENDING' and offer_code='$offer_code'";
		$result = mysqli_query($connection,$query);
		$rows=mysqli_num_rows($result);
		if($rows>0)
			return true;
		else
			return false;

	}

	function minutesElapsed($offer_code,$connection,$mark){

		$query="select substr(time,-13,5) as time_start, substr(time,-5) as time_end from subj_schedule where offer_code='$offer_code'";
		$result = mysqli_query($connection,$query);

		$results = array();
	    while($line = mysqli_fetch_assoc($result)){
	    	$results[] = $line;
	    }

	   	$start_time = strtotime("14:30"); //static
		$current_time = strtotime("14:34"); //static


		$minutes =  round(($current_time - $start_time) / 60,2);
		$message = null;
		if($mark == 'CHECKMARK'){
			if($minutes<5 && $minutes >=0)
				$message= "PRESENT";
			elseif($minutes>=5)
				$message = "LATE";
			
		}
		if($mark == 'XMARK'){
				$message = "PENDING";
		}
		//echo $message;
		return $message;




	}

	function minutesBeforeOut($offer_code,$connection,$mark){

		

		$query="select substr(time,-13,5) as time_start, substr(time,-5) as time_end from subj_schedule where offer_code='$offer_code'";
		$result = mysqli_query($connection,$query);

		$results = array();
	    while($line = mysqli_fetch_assoc($result)){
	    	$results[] = $line;
	    }

	   	$end_time = strtotime("14:00"); //static
		$current_time = strtotime("13:55"); //static

	    //$start_time = strtotime($results[0]['time_start']); //from db
		//$current_time = strtotime(date('G:i'));
		$minutes =  round(($end_time - $current_time) / 60,2);
		$message = null;
		if($mark == 'CHECKMARK'){
			$message= "OK";
		}
		if($mark == 'XMARK'){
			$message= "EARLY DISMISSAL";
		}
		//echo $message;
		return $message;




	}



	function insertReport($offer_code,$connection,$mark){
		$message = $this->minutesElapsed($offer_code,$connection,$mark);
		$countPendingFirstChecked = $this->countPendingFirstChecked($offer_code,$connection);
		$firstChecked = $this->firstChecked($offer_code,$connection);
		$lastChecked = $this->lastChecked($offer_code,$connection);



		$this->checkingStatusCount($offer_code,$connection);

		try{
			$checkingStatus = $this->checkingStatus($offer_code,$connection);
		}
		catch(Exception $e){
			echo "no stat";
		}
	



		if(!$firstChecked && $mark== "CHECKMARK" ) {
			$insert_result = $this->insert_query($connection,$message,$offer_code,$mark);

				
		}



		if(!$countPendingFirstChecked && $mark == "XMARK" && !$firstChecked ){
			$insert_result = $this->insert_query($connection,$message,$offer_code,$mark);

		}

		
		if($firstChecked && !$countPendingFirstChecked && $mark=="XMARK"  && $checkingStatus=='0/2'){
			//echo "update1";

			$message = $this->minutesElapsed($offer_code,$connection,$mark);
			$update_result = $this->update_query($connection,$offer_code,$message);

		}


		if($firstChecked && $countPendingFirstChecked && $mark=="CHECKMARK" && !$lastChecked ){
			$message = $this->minutesElapsed($offer_code,$connection,$mark);
			$update_result = $this->update_query($connection,$offer_code,$message);
		}

		if($firstChecked && $countPendingFirstChecked && $mark=="XMARK" && !$lastChecked ){
			//echo "update1";
			$message = $this->minutesElapsed($offer_code,$connection,$mark);
			$update_result = $this->update_query($connection,$offer_code,'ABSENT');
		}

		if($firstChecked && !$countPendingFirstChecked  && !$lastChecked && $checkingStatus=='1/2'){
			$message2 = $this->minutesBeforeOut($offer_code,$connection,$mark);
			$update_result2 = $this->update_query2($connection,$offer_code,$message2);

		}



	}

	function insert_query($connection,$message,$offer_code,$mark){
		$report_id = $this->generateReportID($connection);
		$message = $this->minutesElapsed($offer_code,$connection,$mark);
		$date = $date =date('Y-m-d');
		$query = "insert into touch_faculty_report (report_id,first_checking,date,offer_code) ";
		$values = "values ('$report_id', '$message' , '$date' , '$offer_code' )";
		$result = mysqli_query($connection,$query.$values);
		echo $message;
		return $result;


	}

	function update_query($connection,$offer_code,$message){
		$query = "update touch_faculty_report set first_checking='$message' where last_checking is null and offer_code='$offer_code'";
		$result = mysqli_query($connection,$query);
		echo $message;
		return $result;
	}

	function update_query2($connection,$offer_code,$message){
		echo $message;
		$query = "update touch_faculty_report set last_checking='$message' where offer_code='$offer_code'";
		$result = mysqli_query($connection,$query);
		return $result;
	}

	function absent_query($connection,$offer_code){


		$query = "update touch_faculty_report set checking_status ='2/2' where offer_code = '$offer_code'";
		echo $query;
		$result = mysqli_query($connection,$query);
		return $result;

	}

	function checkDate_query($connection,$offer_code){

		$date = $date =date('Y-m-d');
		$query = "select * from touch_faculty_report where date='$date' and offer_code='$offer_code' and checking_status='2/2'";
		//echo $query;
		$result = mysqli_query($connection,$query);
		$rows = mysqli_num_rows($result);
		if($rows>0)
			return true;
		else
			false;

	}

}


?>
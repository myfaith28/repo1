<?php

require_once('dbconnect.php');


//select * from subj_schedule where count(offer_code)>0 and offer_code=1319
/*


select count(time) from subj_schedule where time=''
select count(teacher_id) from subj_schedule where teacher_id=''
select count(room_no) from subj_schedule where room_no ''
select count(day) from subj_schedule where day like '%MWF%'
select count(school_year) from subj_schedule where school_year=''






*/


if(isset($_POST['add'])){
	
    $offercode = $_POST['offercode'];
    
    $time = $_POST['start_time']." - ".$_POST['end_time'];
    $teacher = $_POST['teacher_select'];
    $room = $_POST['room'];
    $schoolyear="2013-2014";
    $semester="1";

    foreach($_POST['day'] as $row){
        $day[]=$row;
    }
   
    $plot = new plot_schedule_add();
    $plot->dayQuery($day);
    




}

$plot = new plot_schedule_add();

class plot_schedule_add{


    function dayQuery($day){
        $quer="";
        foreach($day as $row){
            $quer.="day like '%$row%' or ";
        }
        $dayString = substr($query,0,-3);
        $query = "select count(day) as day from subj_schedule where ".$dayString;
        return $query;

    }

    function insertQuery($offercode){
        $db = new dbconnect();
        $connection=$db->connect();
        $query = "select count(*) as count from subj_schedule where offer_code='$offercode'";
        $result = mysqli_query($connection,$query);
        $results = array();
        while($line = mysqli_fetch_assoc($result)){
            $results[] = $line;
        }
        $count = $results[0]['count'];
        $offercode_available=$count < '1' ? true : false;
        if($offercode_available)
            echo "offer code available";
        else
            echo "offer code not avaialble";




    }

    function roomsView(){
        $rooms = array("BCL2","BCL3","BCL4","BCL5","BCL8","BCL9");
        array_push($rooms,"B501","B502","B503","B504","B505","B506");
        $room_opt="";
        for($i=0;$i<sizeof($rooms);$i++){
            $room=$rooms[$i];
            $room_opt.="<option value='$room'>$room</option>";
        }
        echo "<select name=room>";
        echo $room_opt;
        echo "</select>";

    }

    function subjectsModel(){
        $db = new dbconnect();
        $connection=$db->connect();
        $query = "select * from subject";
        $result = mysqli_query($connection,$query);
        $rows=mysqli_num_rows($result); 
        $results = array();
        while($line = mysqli_fetch_assoc($result)){
            $results[] = $line;
        }
        return $results;

    }

    function subjectsView(){
        $subject = $this->subjectsModel();
        $courseno="";
        for($i=0;$i<sizeof($subject);$i++){
            $subject_name = $subject[$i]['subj_name'];
            $subj_id = $subject[$i]['subj_id'];
            $courseno.= "<option value='$subj_id'>".$subject_name."</option>";
        }
        echo $courseno;
    }

    function teachersModel(){
        $db = new dbconnect();
        $connection=$db->connect();
        $query = "select last_name, first_name, t.teacher_id from person p, teacher t where p.person_id=t.person_id";
        $result = mysqli_query($connection,$query);
        $rows=mysqli_num_rows($result); 
        $results = array();
        while($line = mysqli_fetch_assoc($result)){
            $results[] = $line;
        }
        return $results;
    }

    function teachersView(){
 
		$teacher = $this->teachersModel();
    	$names="";

    	for($i=0;$i<sizeof($teacher);$i++){
            $teacher_id = $teacher[$i]['teacher_id'];
			$teachername = $teacher[$i]['last_name']." ".$teacher[$i]['first_name'];
			$names.= "<option value='$teacher_id'>".$teachername."</option>";
		}
        echo "<select name='teacher_select'>";
		echo $names;
        echo "</select>";
    }

    function daysView(){
    	$day_string = array("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
        $day_string_init = array("M","T","W","Th","F","S");
    
    	$days="";
    	for($i=0;$i<sizeof($day_string);$i++){
    		$day = $day_string[$i];
            $day_init = $day_string_init[$i];

    		$days .= "<input name='day[]' type=checkbox value='$day_init' /> $day <br>";
    	}
    	echo $days;
    }
}
?>

<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="dist/bootstrap-clockpicker.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/github.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>


<form method=post>
	<table>
		<tr><td>offer code:</td> <td><input name=offercode></td> </tr>
        <tr><td>subject:</td> <td> <select name='subj_id'> <?php $plot->subjectsView() ?>  </select> </td> </tr>
		<tr><td>day:</td> <td> <?php $plot->daysView() ?> </td></tr>


		<tr><td>time:</td> <td> 

            start<input id="start_time" name="start_time" type="text" class="input-small">
            <span class="add-on">
                <i class="icon-time"></i>
            </span>
            end<input id="end_time" name="end_time" type="text" class="input-small">
            <span class="add-on">
                <i class="icon-time"></i>
            </span>

        </td> </tr>
		<tr><td>teacher:</td>
		<td><?php $plot->teachersView() ?></td> </tr>

        <tr><td>rooms:</td>
        <td><?php $plot->roomsView() ?></td> </tr>

	</table>
	<input type=submit name=add /> 
</form>

<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="dist/bootstrap-clockpicker.min.js"></script>
<script type="text/javascript" src="assets/js/highlight.min.js"></script>


<script type="text/javascript">
var input = $('#start_time,#end_time').clockpicker({
    placement: 'bottom',
    align: 'left',
    autoclose: true,
    'default': 'now'
});

// Manually toggle to the minutes view
$('#check-minutes').click(function(e){
    // Have to stop propagation here
    e.stopPropagation();
    input.clockpicker('show')
            .clockpicker('toggleView', 'minutes');
});
</script>




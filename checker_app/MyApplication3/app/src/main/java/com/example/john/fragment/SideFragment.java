package com.example.john.fragment;


import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.john.myapplication.StaticFunctions;
import com.example.john.myapplication.R;
import com.example.john.sidefragmentclasses.Side;
import com.example.john.sidefragmentclasses.SideAdapter;
import com.example.john.sidefragmentclasses.SideModel;
import com.example.john.webservice.KonektorHTTP;

import java.util.ArrayList;

public class SideFragment extends Fragment {

    private static String HOST = new StaticFunctions().getHost();
    private FragmentManager fm;
    private FragmentTransaction ft;
    private Bundle bundle;
    private SideAdapter adapter;
    private CenterFragment centerfragment;
    private KonektorHTTP konektorweb;
    private static String KONEKTOR_FILE = "konektor.php";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.side_fragment, container, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        new KonektorTask().execute();

    }


    public class KonektorTask extends AsyncTask<String, Void, String[]> {
        @Override
        protected String[] doInBackground(String... params) {
            String fromkonektor[] = new String[2];
            konektorweb = new KonektorHTTP();
            fromkonektor = konektorweb.postData(HOST+KONEKTOR_FILE,"alltime","");
            return fromkonektor;
        }

        @Override
        protected void onPostExecute(String result[]) {
            final ArrayList<SideModel> time = new Side().GetSearchResults(result[0]);
            final ListView listview = (ListView) getView().findViewById(R.id.side_listview);
            listview.setAdapter(new SideAdapter(getActivity(), time));
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                adapter = new SideAdapter(this, time);
                bundle = new Bundle();
                bundle.putString("time",adapter.getItemString(position));
                centerfragment = new CenterFragment();
                centerfragment.setArguments(bundle);
                fm = getFragmentManager();
                ft = fm.beginTransaction();
                ft.replace(R.id.center_container, centerfragment).commit();
                for(int i=0;i<listview.getChildCount();i++){
                    View side_item  =listview.getChildAt(i);
                    TextView t = (TextView) side_item.findViewById(R.id.time);
                    if(i!=position)
                        t.setBackgroundColor(Color.TRANSPARENT);
                    else
                        t.setBackgroundColor(Color.CYAN);
                }
                }
            });
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }


    }

}
package com.example.john.centerfragmentclasses;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by John on 12/22/2014.
 */
public class Center {



    public ArrayList<CenterModel> GetSearchResults(String JSONString){



        ArrayList<CenterModel> results = new ArrayList<CenterModel>();
        CenterModel thistime;
        try {
            JSONArray fulljson = new JSONArray(JSONString);
            for(int i = 0; i<fulljson.length();i++) {

                String json = fulljson.getString(i);
                JSONObject object = new JSONObject(json);

                String offercode = object.getString("offer_code");
                String lastname = object.getString("last_name");
                String firstname = object.getString("first_name");
                String room = object.getString("room_no");


                thistime = new CenterModel();
                thistime.setOffer_code(offercode);
                thistime.setLast_name(lastname+", ");
                thistime.setFirst_name(firstname);
                thistime.setRoom(room);
                results.add(thistime);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }



        return results;
    }
}

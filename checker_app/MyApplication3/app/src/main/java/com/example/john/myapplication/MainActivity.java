package com.example.john.myapplication;

import android.app.Activity;



import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.example.john.fragment.CenterFragment;
import com.example.john.fragment.HeaderFragment;
import com.example.john.fragment.LoginFragment;
import com.example.john.fragment.SideFragment;





public class MainActivity extends FragmentActivity {


    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String PREFNAME="MyPref";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_screen);
        CenterFragment centerfragment = new CenterFragment();
        LoginFragment loginfragment = new LoginFragment();
        pref = getApplicationContext().getSharedPreferences(PREFNAME, 0);
        editor = pref.edit();
        if(pref.getString("last_name","").equals(""))
            getSupportFragmentManager().beginTransaction().add(R.id.login_container, loginfragment).commit();

        else
            loadFragments();



    }


    public void loadFragments(){
        SideFragment sidefragment = new SideFragment();
        HeaderFragment headerfragment = new HeaderFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.header_container, headerfragment).commit();
        getSupportFragmentManager().beginTransaction().add(R.id.side_container, sidefragment).commit();
    }

    public void lockDrawer(Boolean val){
        DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
        if(val)
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        else
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }





}

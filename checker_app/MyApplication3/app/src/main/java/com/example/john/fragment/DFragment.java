package com.example.john.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.example.john.myapplication.R;
import com.example.john.myapplication.StaticFunctions;
import com.example.john.webservice.KonektorHTTP;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DFragment extends DialogFragment {
    private static String HOST = new StaticFunctions().getHost();
    private static String DIALOG_TEXTS = "dialog_texts";
    private static String OFFERS_FILE = "offers.php";
    private static String FACULTY_REPORTS_FILE = "faculty_reports1.php";
    private static String CHECKMARK = "CHECKMARK";
    private static String XMARK = "XMARK";
    private static String SAVE = "SAVE";
    private ImageButton checkmark;
    private ImageButton xmark;
    private Button save;
    private KonektorHTTP konektorweb;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.checker_dialog, container,
                false);
        getDialog().setTitle("Dialog");
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){

        super.onActivityCreated(savedInstanceState);
        Bundle b = this.getArguments();
        final String offercode_argument = b.getString("offer_code_centerfragment");
        new Konektor().execute(offercode_argument,DIALOG_TEXTS);
        checkmark = (ImageButton)getView().findViewById(R.id.checkmark);
        xmark = (ImageButton)getView().findViewById(R.id.xmark);
        save = (Button)getView().findViewById(R.id.save);

        checkmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new actionTask().execute(offercode_argument,CHECKMARK);
            }
        });

        xmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new actionTask().execute(offercode_argument,XMARK);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new actionTask().execute(offercode_argument,SAVE);
            }
        });





    }

    private void disabler(){

        checkmark = (ImageButton)getView().findViewById(R.id.checkmark);
        xmark = (ImageButton)getView().findViewById(R.id.xmark);
        save = (Button)getView().findViewById(R.id.save);

        checkmark.setEnabled(false);
        xmark.setEnabled(false);
        save.setEnabled(false);

        checkmark.setImageResource(R.drawable.checkmark_gray);
        xmark.setImageResource(R.drawable.xmark_gray);


    }

    public class Konektor extends AsyncTask<String, Void, ArrayList<NameValuePair>> {
        @Override
        protected ArrayList<NameValuePair> doInBackground(String... params) {
            ArrayList<NameValuePair> myList = new ArrayList<NameValuePair>();
            String offercode = params[0];
            String hostoffers = HOST + OFFERS_FILE;
            String hostreports = HOST + FACULTY_REPORTS_FILE;
            String fromkonektor[] = new String[2];
            String enable_checking[] = new String[2];
            KonektorHTTP konektorweb = new KonektorHTTP();
            try {

                fromkonektor = konektorweb.postData(hostoffers, "offers", offercode);
                enable_checking = konektorweb.postData(hostreports,"enable_details", offercode);


            }

            catch (Exception e) {
            }
            myList.add(new BasicNameValuePair("offer_details",fromkonektor[0]));
            myList.add(new BasicNameValuePair("enable_details",enable_checking[0]));

            return myList;
        }

        @Override
        protected void onPostExecute(ArrayList<NameValuePair> listresult) {
            setDialogTexts(listresult.get(0).getValue());
            String enabler_result = listresult.get(1).getValue();
            Toast.makeText(getActivity(),enabler_result+"",Toast.LENGTH_SHORT).show();
            if(!enabler_result.equals("enabled"))
                disabler();

        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }

        private void setDialogTexts(String jsonresult){
            Boolean correct = false;
            try {
                JSONArray fulljson = new JSONArray(jsonresult);

                String json = fulljson.getString(0);
                JSONObject object = new JSONObject(json);
                if(fulljson.length()>0) {
                    correct=true;

                    String offercode_string = object.getString("offer_code");
                    TextView offercode = (TextView)getView().findViewById(R.id.d_offercode);
                    offercode.setText(offercode_string);

                    String courseno_string = object.getString("subj_name");
                    TextView courseno = (TextView)getView().findViewById(R.id.d_courseno);
                    courseno.setText(courseno_string);

                    String coursedesc_string = object.getString("subj_desc");
                    TextView coursedesc = (TextView)getView().findViewById(R.id.d_coursedesc);
                    coursedesc.setText(coursedesc_string);

                    String roomno_string = object.getString("room_no");
                    TextView roomno = (TextView)getView().findViewById(R.id.d_room);
                    roomno.setText(roomno_string);

                    String day_string = object.getString("day");
                    TextView day = (TextView)getView().findViewById(R.id.d_day);
                    day.setText(day_string);

                    String time_string = object.getString("time");
                    TextView time = (TextView)getView().findViewById(R.id.d_time);
                    time.setText(time_string);

                    String lastname_string = object.getString("last_name");
                    TextView lastname = (TextView)getView().findViewById(R.id.d_lastname);
                    lastname.setText(lastname_string);

                    String firstname_string = object.getString("first_name");
                    TextView firstname = (TextView)getView().findViewById(R.id.d_firstname);
                    firstname.setText(firstname_string);

                    String collegename_string = object.getString("college_name");
                    TextView collegename = (TextView)getView().findViewById(R.id.d_college);
                    collegename.setText(collegename_string);

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }

    public class actionTask extends AsyncTask<String, Void, String[]>{

        @Override
        protected String[] doInBackground(String... params) {
            String hostreports = HOST + FACULTY_REPORTS_FILE;
            String offercode = params[0];
            String action = params[1];
            String faculty_report_result[] = new String[2];
            konektorweb = new KonektorHTTP();
            faculty_report_result=konektorweb.postData(hostreports, "faculty_reports1", offercode,action);



            return faculty_report_result;
        }
        @Override
        protected void onPostExecute(String[] result){

            Log.e("test1", result[0]);
            Log.e("test2",result[1]);

            Toast.makeText(getActivity(),"<result>"+result[0]+" <result>",Toast.LENGTH_SHORT).show();
            if(result[0].equals("done") || result[0].equals("half done"))  {
                disabler();
                getDialog().dismiss();

            }



        }

    }



}

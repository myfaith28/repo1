package com.example.john.fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import com.example.john.centerfragmentclasses.Center;
import com.example.john.centerfragmentclasses.CenterAdapter;
import com.example.john.centerfragmentclasses.CenterModel;
import com.example.john.myapplication.StaticFunctions;
import com.example.john.myapplication.R;
import com.example.john.webservice.KonektorHTTP;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import java.util.ArrayList;

public class CenterFragment extends Fragment {
    private static String host = new StaticFunctions().getHost();
    private static String KONEKTOR_FILE = "konektor.php";
    private static String GET_DAY = "getday.php";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.center_fragment, container, false);
        return rootView;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        Bundle timebundle = this.getArguments();
        String time = timebundle.getString("time");
        new Konektor().execute(time);

    }


    public class Konektor extends AsyncTask<String, Void, ArrayList<NameValuePair>> {
        @Override
        protected ArrayList<NameValuePair> doInBackground(String... params) {

            ArrayList<NameValuePair> myList = new ArrayList<NameValuePair>();
            String hostkonektor = host+KONEKTOR_FILE;
            String hostgetday = host+GET_DAY;
            String fromkonektor[] = new String[2];
            String fromgetday[] = new String[2];
            String time = null;
            KonektorHTTP konektorweb = new KonektorHTTP();
            try {
                fromkonektor = konektorweb.postData(hostkonektor,"time",params[0]);
                fromgetday = konektorweb.postData(hostgetday,"","");
                time = params[0];
            }
            catch (Exception e){

            }
            myList.add(new BasicNameValuePair("classes_where_time",fromkonektor[0]));
            myList.add(new BasicNameValuePair("classes_where_time_status",fromkonektor[1]));
            myList.add(new BasicNameValuePair("getday",fromgetday[0]));
            myList.add(new BasicNameValuePair("getday_status",fromgetday[1]));
            myList.add(new BasicNameValuePair("timedisplay",time));
            return myList;
        }

        @Override
        protected void onPostExecute(ArrayList<NameValuePair> listresult) {

            String classes_json = listresult.get(0).getValue();
            String today = listresult.get(2).getValue();
            String target_time = listresult.get(4).getValue();

            //Toast.makeText(getActivity(), classes_json, Toast.LENGTH_SHORT).show();
            TextView dayoftheweek = (TextView)getView().findViewById(R.id.daytoday);
            dayoftheweek.setText(today+" classes "+target_time);


            final ArrayList<CenterModel> offers = new Center().GetSearchResults(classes_json);
            final GridView listview = (GridView) getView().findViewById(R.id.center_listview);
            listview.setAdapter(new CenterAdapter(getActivity(), offers));

            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                    CenterAdapter ca = new CenterAdapter(this,offers);
                    String offercode = ca.getOC(position);
                    Bundle b = new Bundle();
                    b.putString("offer_code_centerfragment",offercode);
                    DFragment df = new DFragment();
                    df.setArguments(b);
                    FragmentManager fm = getFragmentManager();
                    df.setRetainInstance(true);
                    df.show(fm, "fragment_name");

                }
            });

        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }


    }

    public class timePickerTask extends AsyncTask <String, Void, String>{
        @Override
        protected String doInBackground(String... params) {


            return "";
        }

        @Override
        protected void onPostExecute(String result) {



        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }


    }

}

//



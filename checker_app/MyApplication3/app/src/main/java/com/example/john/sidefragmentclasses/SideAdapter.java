package com.example.john.sidefragmentclasses;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.john.myapplication.R;

import java.util.ArrayList;

/**
 * Created by John on 12/22/2014.
 */
public class SideAdapter extends BaseAdapter {

    private static ArrayList<SideModel> time_arraylist;
    private LayoutInflater l_Inflater;

    public SideAdapter(Context context, ArrayList<SideModel> results) {
        time_arraylist = results;
        l_Inflater = LayoutInflater.from(context);
    }

    public SideAdapter(Object context, Object results) {
        // TODO Auto-generated constructor stub
    }

    public int getCount() {
        return time_arraylist.size();
    }

    public Object getItem(int position) {

        return time_arraylist.get(position);
    }

    public String getItemString(int position){
        return time_arraylist.get(position).getTime();
    }



    public long getItemId(int position) {
        return position;
    }



    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;


        if (convertView == null) {
            convertView = l_Inflater.inflate(R.layout.side_item, null);
            holder = new ViewHolder();
            holder.time = (TextView) convertView.findViewById(R.id.time);



            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.time.setText(time_arraylist.get(position).getTime());


        //holder.time.setBackgroundColor(time_arraylist.get(1).getColor());


        return convertView;
    }

    static class ViewHolder {
        TextView time;


    }
}

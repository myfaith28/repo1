package com.example.john.fragment;




import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.john.myapplication.MainActivity;
import com.example.john.myapplication.R;
import com.example.john.myapplication.StaticFunctions;
import com.example.john.webservice.KonektorHTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginFragment extends Fragment {
    private static String HOST = new StaticFunctions().getHost();
    private static String AUTH_FILE = "auth.php";
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String PREFNAME="MyPref";
    private LoginFragment loginfragment;
    private SideFragment sidefragment;
    private CenterFragment centerfragment;
    private HeaderFragment headerfragment;
    private FragmentManager fm;
    private FragmentTransaction ft;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.login_fragment, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((MainActivity)getActivity()).lockDrawer(true);

        pref = getActivity().getSharedPreferences("MyPref", 0);
        editor = pref.edit();
        Button login_btn = (Button)getView().findViewById(R.id.login_btn);
        login_btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                EditText username = (EditText)getView().findViewById(R.id.readslogin);
                EditText password = (EditText)getView().findViewById(R.id.readspassword);
                String user = username.getText().toString();
                String pass = password.getText().toString();
                editor.putString("login",user);
                editor.commit();
                new LoginTask().execute(user, pass);

            }
        });

    }

    public class LoginTask extends AsyncTask<String, Void, String[]> {
        @Override
        protected String[] doInBackground(String... params) {
            String hostauthlogin = HOST+AUTH_FILE;
            String fromkonektor[]= new String[2];
            KonektorHTTP konektorweb = new KonektorHTTP();
            try {
                fromkonektor = konektorweb.postData(hostauthlogin,"auth",params[0],params[1]);
            }
            catch (Exception e){
            }
            return fromkonektor;
        }

        @Override
        protected void onPostExecute(String result[]) {
            Log.e("test1",result[0]);
            Log.e("test2", result[1]);
            Boolean login = false;
            Boolean error = result[1].equals("error") ? true : false;
            loginfragment = new LoginFragment();
            sidefragment = new SideFragment();
            headerfragment = new HeaderFragment();
            fm = getFragmentManager();
            ft = fm.beginTransaction();

            if(!error) {
                if(storeJSONToBundle(result[0]))
                    login=true;
                else
                    Toast.makeText(getActivity(),"Wrong password", Toast.LENGTH_SHORT).show();
            }
            else
                Toast.makeText(getActivity(),"Server error", Toast.LENGTH_SHORT).show();

            if(login) {

                ft.replace(R.id.login_container, loginfragment);
                ft.remove(loginfragment);
                ft.replace(R.id.header_container, headerfragment);
                ft.add(R.id.side_container, sidefragment);
                ft.commit();
            }

        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
        private Boolean storeJSONToBundle(String jsonresult){
            Boolean correct = false;
            try {
                JSONArray fulljson = new JSONArray(jsonresult);

                String json = fulljson.getString(0);
                JSONObject object = new JSONObject(json);
                if(fulljson.length()>0) {
                    correct=true;
                    String lastname = object.getString("last_name");
                    String firstname = object.getString("first_name");
                    editor = pref.edit();
                    editor.putString("first_name", firstname);
                    editor.putString("last_name", lastname);
                    editor.commit();
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
            return correct;
        }
    }
}

-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 23, 2015 at 12:24 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `touchims`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_function`
--

CREATE TABLE IF NOT EXISTS `access_function` (
`access_function_id` int(11) NOT NULL,
  `access_id` int(11) NOT NULL,
  `func_id` int(11) NOT NULL,
  `status` varchar(15) DEFAULT 'Pending'
) ENGINE=MyISAM AUTO_INCREMENT=322 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `access_function`
--

INSERT INTO `access_function` (`access_function_id`, `access_id`, `func_id`, `status`) VALUES
(49, 6, 130, 'Pending'),
(50, 6, 131, 'Pending'),
(51, 6, 132, 'Pending'),
(52, 6, 133, 'Pending'),
(53, 6, 134, 'Pending'),
(54, 4, 169, 'Active'),
(55, 4, 170, 'Active'),
(56, 4, 172, 'Active'),
(57, 4, 173, 'Active'),
(58, 4, 174, 'Active'),
(59, 4, 175, 'Active'),
(60, 4, 176, 'Active'),
(61, 4, 177, 'Active'),
(62, 4, 178, 'Active'),
(63, 4, 179, 'Active'),
(64, 4, 180, 'Active'),
(65, 4, 181, 'Active'),
(66, 4, 182, 'Active'),
(67, 4, 183, 'Active'),
(68, 4, 184, 'Active'),
(69, 4, 185, 'Active'),
(70, 4, 186, 'Active'),
(71, 4, 187, 'Active'),
(72, 4, 188, 'Active'),
(73, 4, 189, 'Active'),
(74, 4, 190, 'Active'),
(75, 4, 191, 'Active'),
(76, 4, 192, 'Active'),
(77, 4, 193, 'Active'),
(78, 4, 194, 'Active'),
(79, 4, 195, 'Active'),
(80, 4, 196, 'Active'),
(81, 4, 197, 'Active'),
(82, 4, 198, 'Active'),
(83, 4, 199, 'Active'),
(84, 4, 200, 'Active'),
(85, 4, 201, 'Active'),
(86, 4, 202, 'Active'),
(87, 4, 203, 'Active'),
(88, 4, 204, 'Active'),
(89, 4, 205, 'Active'),
(90, 4, 206, 'Active'),
(91, 4, 207, 'Active'),
(92, 4, 208, 'Active'),
(93, 4, 209, 'Active'),
(94, 4, 210, 'Active'),
(95, 4, 211, 'Active'),
(96, 4, 212, 'Active'),
(97, 4, 213, 'Active'),
(98, 4, 214, 'Active'),
(99, 4, 215, 'Active'),
(100, 4, 146, 'Active'),
(101, 4, 147, 'Active'),
(102, 4, 148, 'Active'),
(103, 4, 149, 'Active'),
(104, 4, 150, 'Active'),
(105, 4, 151, 'Active'),
(106, 4, 152, 'Active'),
(107, 4, 153, 'Active'),
(108, 4, 154, 'Active'),
(109, 4, 155, 'Active'),
(110, 4, 156, 'Active'),
(111, 4, 157, 'Active'),
(112, 4, 158, 'Active'),
(113, 4, 159, 'Active'),
(114, 4, 160, 'Active'),
(115, 4, 161, 'Active'),
(116, 4, 162, 'Active'),
(117, 4, 163, 'Active'),
(118, 4, 164, 'Active'),
(119, 4, 165, 'Active'),
(120, 4, 166, 'Active'),
(121, 4, 167, 'Active'),
(122, 4, 168, 'Active'),
(123, 4, 141, 'Active'),
(124, 4, 142, 'Active'),
(125, 4, 143, 'Active'),
(126, 4, 144, 'Active'),
(127, 4, 145, 'Active'),
(128, 4, 135, 'Active'),
(129, 4, 136, 'Active'),
(130, 4, 137, 'Active'),
(131, 4, 138, 'Active'),
(132, 4, 139, 'Active'),
(133, 4, 140, 'Active'),
(134, 4, 130, 'Active'),
(135, 4, 131, 'Active'),
(136, 4, 132, 'Active'),
(137, 4, 133, 'Active'),
(138, 4, 134, 'Active'),
(139, 4, 216, 'Active'),
(140, 5, 169, 'Active'),
(141, 5, 170, 'Active'),
(142, 5, 172, 'Active'),
(143, 5, 173, 'Active'),
(144, 5, 174, 'Active'),
(145, 5, 175, 'Active'),
(146, 5, 176, 'Active'),
(147, 5, 177, 'Active'),
(148, 5, 178, 'Active'),
(149, 5, 179, 'Active'),
(150, 5, 180, 'Active'),
(151, 5, 181, 'Active'),
(152, 5, 182, 'Active'),
(153, 5, 183, 'Active'),
(154, 5, 184, 'Active'),
(155, 5, 185, 'Active'),
(156, 5, 186, 'Active'),
(157, 5, 187, 'Active'),
(158, 5, 188, 'Active'),
(159, 5, 189, 'Active'),
(160, 5, 216, 'Active'),
(161, 5, 190, 'Active'),
(162, 5, 191, 'Active'),
(163, 5, 192, 'Active'),
(164, 5, 193, 'Active'),
(165, 5, 194, 'Active'),
(166, 5, 195, 'Active'),
(167, 5, 196, 'Active'),
(168, 5, 197, 'Active'),
(169, 5, 198, 'Active'),
(170, 5, 199, 'Active'),
(171, 5, 200, 'Active'),
(172, 5, 201, 'Active'),
(173, 5, 202, 'Active'),
(174, 5, 203, 'Active'),
(175, 5, 204, 'Active'),
(176, 5, 205, 'Active'),
(177, 5, 206, 'Active'),
(178, 5, 207, 'Active'),
(179, 5, 208, 'Active'),
(180, 5, 209, 'Active'),
(181, 5, 210, 'Active'),
(182, 5, 211, 'Active'),
(183, 5, 212, 'Active'),
(184, 5, 213, 'Active'),
(185, 5, 214, 'Active'),
(186, 5, 215, 'Active'),
(187, 5, 146, 'Active'),
(188, 5, 147, 'Active'),
(189, 5, 148, 'Active'),
(190, 5, 149, 'Active'),
(191, 5, 150, 'Active'),
(192, 5, 151, 'Active'),
(193, 5, 152, 'Active'),
(194, 5, 153, 'Active'),
(195, 5, 154, 'Active'),
(196, 5, 155, 'Active'),
(197, 5, 156, 'Active'),
(198, 5, 157, 'Active'),
(199, 5, 158, 'Active'),
(200, 5, 159, 'Active'),
(201, 5, 160, 'Active'),
(202, 5, 161, 'Active'),
(203, 5, 162, 'Active'),
(204, 5, 163, 'Active'),
(205, 5, 164, 'Active'),
(206, 5, 165, 'Active'),
(207, 5, 166, 'Active'),
(208, 5, 167, 'Active'),
(209, 5, 168, 'Active'),
(210, 5, 217, 'Active'),
(211, 5, 141, 'Active'),
(212, 5, 142, 'Active'),
(213, 5, 143, 'Active'),
(214, 5, 144, 'Active'),
(215, 5, 145, 'Active'),
(216, 5, 135, 'Active'),
(217, 5, 136, 'Active'),
(218, 5, 137, 'Active'),
(219, 5, 138, 'Active'),
(220, 5, 139, 'Active'),
(221, 5, 140, 'Active'),
(222, 5, 130, 'Active'),
(223, 5, 131, 'Active'),
(224, 5, 132, 'Active'),
(225, 5, 133, 'Active'),
(226, 5, 134, 'Active'),
(254, 6, 141, 'Pending'),
(255, 6, 142, 'Pending'),
(256, 6, 143, 'Pending'),
(257, 6, 144, 'Pending'),
(258, 6, 145, 'Pending'),
(259, 6, 135, 'Pending'),
(260, 6, 136, 'Pending'),
(261, 6, 137, 'Pending'),
(262, 6, 138, 'Pending'),
(263, 6, 139, 'Pending'),
(264, 6, 140, 'Pending'),
(265, 7, 130, 'Active'),
(266, 7, 131, 'Active'),
(267, 7, 132, 'Active'),
(268, 7, 133, 'Active'),
(269, 7, 134, 'Active'),
(270, 8, 130, 'Active'),
(271, 8, 131, 'Active'),
(272, 8, 132, 'Active'),
(273, 8, 133, 'Active'),
(274, 8, 134, 'Active'),
(275, 9, 141, 'Pending'),
(276, 9, 142, 'Pending'),
(277, 9, 143, 'Pending'),
(278, 9, 144, 'Pending'),
(279, 9, 145, 'Pending'),
(280, 9, 135, 'Pending'),
(281, 9, 136, 'Pending'),
(282, 9, 137, 'Pending'),
(283, 9, 138, 'Pending'),
(284, 9, 139, 'Pending'),
(285, 9, 140, 'Pending'),
(286, 9, 130, 'Pending'),
(287, 9, 131, 'Pending'),
(288, 9, 132, 'Pending'),
(289, 9, 133, 'Pending'),
(290, 9, 134, 'Pending'),
(291, 2, 190, 'Active'),
(292, 2, 191, 'Active'),
(293, 2, 192, 'Active'),
(294, 2, 193, 'Active'),
(295, 2, 194, 'Active'),
(296, 2, 195, 'Active'),
(297, 2, 196, 'Active'),
(298, 2, 197, 'Active'),
(299, 2, 198, 'Active'),
(300, 2, 199, 'Active'),
(301, 2, 200, 'Active'),
(302, 2, 201, 'Active'),
(303, 2, 202, 'Active'),
(304, 2, 203, 'Active'),
(305, 2, 204, 'Active'),
(306, 2, 205, 'Active'),
(307, 2, 206, 'Active'),
(308, 2, 207, 'Active'),
(309, 2, 208, 'Active'),
(310, 2, 209, 'Active'),
(311, 2, 210, 'Active'),
(312, 2, 211, 'Active'),
(313, 2, 212, 'Active'),
(314, 2, 213, 'Active'),
(315, 2, 214, 'Active'),
(316, 2, 215, 'Active'),
(317, 2, 130, 'Active'),
(318, 2, 131, 'Active'),
(319, 2, 132, 'Active'),
(320, 2, 133, 'Active'),
(321, 2, 134, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
`account_no` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `username` int(11) NOT NULL,
  `password` varchar(30) NOT NULL,
  `email_address` varchar(45) NOT NULL,
  `file_name` text NOT NULL,
  `full_path` text NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '0',
  `user` varchar(10) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`account_no`, `person_id`, `username`, `password`, `email_address`, `file_name`, `full_path`, `user_type`, `user`) VALUES
(1, 1, 2006009003, 'cGFzc3dvcmQ=', 'anecitagabisan@gmail.com', '2006009003.jpg', 'D:/dev/www/konektor_in/uploads/prof_pic/2006009003.jpg', 0, 'student'),
(2, 4, 1000000001, 'cGFzc3dvcmRfYWRtaW4=', 'projectkonektor@gmail.com', 'temp.jpg', 'D:/dev/www/Konektor/uploads/prof_pic/temp.jpg', 1, 'student');

-- --------------------------------------------------------

--
-- Table structure for table `attempts`
--

CREATE TABLE IF NOT EXISTS `attempts` (
`attempt_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `id_number` int(11) NOT NULL,
  `no_of_attempts` int(11) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attempts`
--

INSERT INTO `attempts` (`attempt_id`, `person_id`, `id_number`, `no_of_attempts`) VALUES
(4, 2, 2006007993, 4),
(5, 140, 2001001001, 1);

-- --------------------------------------------------------

--
-- Table structure for table `college`
--

CREATE TABLE IF NOT EXISTS `college` (
`college_id` int(11) NOT NULL,
  `college_name` text NOT NULL,
  `dean_name` varchar(45) NOT NULL,
  `location` text
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `college`
--

INSERT INTO `college` (`college_id`, `college_name`, `dean_name`, `location`) VALUES
(1, 'COLLEGE OF INFORMATION COMPUTER AND COMMUNICATION TECHNOLOGY', 'GREGG VICTOR GABISON', 'Basak'),
(15, 'COLLEGE OF COMMERCE', 'DANIEL GOLAMAN', 'Main'),
(16, 'COLLEGE OF ARTS AND SCIENCES', 'HANNAH MICHELLE OEN', 'Main'),
(17, 'COLLEGE OF ENGINEERING', 'MARC VERANO', 'Main'),
(18, 'COLLEGE OF EDUCATION', 'MELLISA GALLOSA', 'Basak'),
(19, 'COLLEGE OF NURSING', 'AIBIE QUITO', 'basak'),
(20, 'COLLEGE OF LAW', 'CRISTIAN CAUMERAN', 'basak'),
(21, 'COLLEGE OF ARCHIE', 'ARCHIE', 'BASAK');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
`course_id` int(11) NOT NULL,
  `college_id` int(11) DEFAULT NULL,
  `course_abbr` varchar(15) NOT NULL,
  `course_name` text NOT NULL,
  `course_desc` text
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`course_id`, `college_id`, `course_abbr`, `course_name`, `course_desc`) VALUES
(1, 1, 'BSIT', 'BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY', 'TECHNOLOGY'),
(25, 1, 'BSCS', 'BACHELOR OF SCIENCE IN COMPUTER SCIENCE', 'TECHNOLOGY'),
(26, 1, 'BSIS', 'BACHELOR OF SCIENCE IN INFORMATION SYSTEM', 'TECHNOLOGY'),
(27, 1, 'ACT', 'ASSOCIATE IN TECHNOLOGY', 'TECHNOLOGY'),
(28, 19, 'BSN', 'BACHELOR OF SCIENCE IN NURSING', 'MEDICAL'),
(29, 17, 'BSEE', 'BACHELOR OF SCIENCE IN ELECTRICAL ENGINEERING', 'ELECTRONICS'),
(30, 17, 'BSCE', 'BACHELOR OF SCIENCE IN CIVIL ENGINEERING', 'BUILDINGS'),
(31, 17, 'BSECE', 'BACHELOR OF SCIENCE IN ELECTRONICS AND COMMUNICATIONS ENGINEERING', 'ELECTRICAL'),
(32, 17, 'BSEvE', 'BACHELOR OF SCIENCE IN ENVIRONMENTAL ENGINEERING', 'ENVIRONMENT'),
(33, 17, 'BSIE', 'BACHELOR OF SCIENCE IN INDUSTRIAL ENGINEERING', 'INDUSTRIAL'),
(34, 17, 'BSME', 'BACHELOR OF SCIENCE IN MECHANICAL ENGINEERING', 'MECHANICAL'),
(35, 17, 'BSComE', 'BACHELOR OF SCIENCE IN COMPUTER ENGINEERING', 'COMPUTER'),
(36, 16, 'BAEng', 'BACHELOR OF ARTS IN ENGLISH', 'ENGLISH'),
(37, 16, 'BALIACOM', 'BACHELOR OF ARTS IN LIACOM', 'LIACOM'),
(38, 16, 'BAMC', 'BACHELOR OF ARTS IN MASS COMMUNICATION', 'TV AND RADIO'),
(39, 16, 'BAP', 'BACHELOR OF ARTS IN PHILOSOPHY', 'PHILO'),
(40, 16, 'BAPS', 'BACHELOR OF ARTS IN POLITICAL SCIENCE', 'POLITICS'),
(41, 20, 'BAL', 'BACHELOR OF LAW', 'LAW'),
(42, 15, 'BSC', 'BACHELOR OF SCIENCE IN COMMERCE', 'COMMERCE'),
(43, 15, 'BSA', 'BACHELOR OF SCIENCE IN ACCOUNTANCY', 'ACCOUNTANT'),
(44, 15, 'BSHRM', 'BACHELOR OF SCIENCE IN HOTEL AND RESTAURANT MANAGEMENT', 'MANAGER'),
(45, 15, 'BST', 'BACHELOR OF SCIENCE IN TOURISM', 'TOURIST'),
(46, 18, 'BSEd', 'BACHELOR OF SCIENCE IN EDUCATION', 'EDUCATION');

-- --------------------------------------------------------

--
-- Table structure for table `db_access`
--

CREATE TABLE IF NOT EXISTS `db_access` (
`access_id` int(11) NOT NULL,
  `proj_id` int(11) NOT NULL,
  `access_code` text NOT NULL,
  `status` varchar(45) DEFAULT 'Pending'
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_access`
--

INSERT INTO `db_access` (`access_id`, `proj_id`, `access_code`, `status`) VALUES
(2, 2, '24012296-EA17-747E-1A77-B2B4125EA519', 'Active'),
(3, 3, '9F4038FF-C860-EA5A-4295-BFE05B610676', 'Active'),
(4, 4, 'D32AECF3-B056-D68B-B877-D2B077F4D058', 'Active'),
(5, 5, 'D62CA21F-6497-5880-E5D5-B09923D56980', 'Active'),
(6, 6, '320BD57D-9F00-119F-CE19-D5B2E20F3E0B', 'Active'),
(7, 7, '31F8CD09-4AC0-D356-F28A-387690D0D12F', 'Active'),
(8, 8, '559886D4-62CB-321F-8440-13E8A77FA3AC', 'Active'),
(9, 9, 'E49DBE1D-42FE-5078-D809-7657F19D1440', 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `dept_id` int(11) NOT NULL,
  `dept_name` varchar(45) NOT NULL,
  `dept_head` varchar(45) NOT NULL,
  `location` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dept_id`, `dept_name`, `dept_head`, `location`) VALUES
(1001, 'STUDENT AFFAIRS OFFICE', 'YUMANG', 'BASAK AND MAIN'),
(1002, 'STUDENT DEVELOPMENT PLACEMENT CENTER', 'BERONQUE', 'BASAK AND MAIN'),
(1003, 'SAFETY AND SECURITY DEPARTMENT', 'SEVILLA', 'BASAK AND MAIN'),
(1004, 'ARTS AND MUSIC', 'SALIPONG', 'MAIN'),
(1005, 'FACULTY FOUNDATION INCORPORATED', 'MIRO', 'MAIN'),
(1006, 'SUPREME STUDENT COUNCIL', 'VELEZ', 'MAIN'),
(1007, 'ATHLETICS', 'GABISAN', 'BASAK'),
(1008, 'CAMPUS MINISTRY OFFICE', 'OEN', 'MAIN'),
(1009, 'INSTRUCTIONAL PLACEMENT CENTER', 'LOPERNES', 'BASAK AND MAIN'),
(1010, 'RECORDS DIVISION', 'OBREGON', 'BASAK AND MAIN'),
(1011, 'PROPERTY ADMINISTRATION', 'PATERES', 'BASAK AND MAIN'),
(1012, 'CLINIC', 'SIACOR', 'BASAK AND MAIN'),
(1013, 'BOOKSTORE', 'LANSANG', 'BASAK AND MAIN'),
(1014, 'CANTEEN', 'CAINTAPAN', 'BASAK AND MAIN'),
(1015, 'COMMUNITY OUTREACH PROGRAM', 'MATUGAS', 'BASAK'),
(1016, 'PHOTO SECTION', 'CANONIGO', 'BASAK'),
(1017, 'ACCOUNTING DIVISION', 'GO', 'BASAK AND MAIN'),
(1018, 'RESEARCH CENTER', 'DEIPARINI', 'MAIN'),
(1019, 'ELECTRONIC DATA PROCESSING CENTER', 'DE LOS REYES', 'MAIN'),
(1020, 'HUMAN RESOURCE MANAGEMENT OFFICE', 'EMIA', 'MAIN');

-- --------------------------------------------------------

--
-- Table structure for table `function`
--

CREATE TABLE IF NOT EXISTS `function` (
`func_id` int(11) NOT NULL,
  `screen_name` varchar(45) NOT NULL,
  `func_name` text NOT NULL,
  `func_desc` text NOT NULL,
  `func_url` text NOT NULL,
  `func_parameter` text NOT NULL,
  `func_return` text NOT NULL,
  `table_id` int(11) NOT NULL,
  `note` text
) ENGINE=MyISAM AUTO_INCREMENT=218 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `function`
--

INSERT INTO `function` (`func_id`, `screen_name`, `func_name`, `func_desc`, `func_url`, `func_parameter`, `func_return`, `table_id`, `note`) VALUES
(130, 'College.viewAll', 'view_college', 'Returns all the information of all the college in USJR.', 'college_json/view_college', 'access_code : the code given to access', 'college_id, college_name, dean_name, location', 1, NULL),
(131, 'College.viewByLocation', 'view_college_by_location', 'Returns all the information of the college filtered by location', 'college_json/view_college_by_location', 'access_code: the code given to access the function, value: value of location to be searched.', 'college_id, college_name, dean_name, location', 1, NULL),
(132, 'College.viewByName', 'view_college_by_name', 'Returns all the information of the college filtered by college name', 'college_json/view_college_by_name', 'access_code: the code given to access the function, value: value of college name to be searched.', 'college_id, college_name, dean_name, location', 1, NULL),
(133, 'College.viewByDeanName', 'view_college_by_deanName', 'Returns all the information of the college filtered by Dean name', 'college_json/view_college_by_deanName', 'access_code: the code given to access the function, value: value of dean name to be searched.', 'college_id, college_name, dean_name, location', 1, NULL),
(134, 'College.filterAll', 'view_college_filterAll', 'Returns all the information of the college filtered by college name, dean name and location', 'college_json/view_college_filterAll', 'access_code: the code given to access the function, value1: value of college name to be searched, value2: value of dean name to be searched, value3: value of location to be searched', 'college_id, college_name, dean_name, location', 1, NULL),
(135, 'Course.viewAll', 'view_course', 'Returns all the information of all courses.', 'course_json/view_course', 'access_code: the code to access', 'course_id, college_id, college_name, course_abbr,course_name, course_desc', 2, NULL),
(136, 'Course.viewByCourseName', 'view_course_by_name', 'Returns all the information of all courses filtered by course name.', 'course_json/view_course_by_name', 'access_code:the code given to access, value: value of course name to be searched.', 'course_id, college_name, course_abbr, course_name, course_desc', 2, NULL),
(137, 'Course.viewByCourseAbbr', 'view_course_by_abbr', 'Returns all the information of all courses filtered by course abbreviation.', 'course_json/view_course_by_abbr', 'access_code:the code given to access, value: value of course abbreviation to be searched.', 'course_id, college_name, course_abbr, course_name, course_desc', 2, NULL),
(138, 'Course.viewByCourseDesc', 'view_course_by_desc', 'Returns all the information of all courses filtered by course description', 'course_json/view_course_by_desc', 'access_code:the code given to access, value: value of course description to be searched', 'course_id, college_name, course_abbr, course_name, course_desc', 2, NULL),
(139, 'Course.viewByCollegeName', 'view_course_by_college', 'Returns all the information of all courses filtered by college name', 'course_json/view_course_by_college', 'access_code:the code given to access, value: value of college name to be searched', 'course_id, college_name, course_abbr, course_name, course_desc', 2, NULL),
(140, 'Course.filterAll', 'view_course_filterAll', 'Returns all the information of all courses filtered by course abbreviation, name, description, college name', 'course_json/view_course_filterAll', 'access_code:the code given to access, value1: value of course abbreviation, value2: value of course name, value3: value of course description, value4: value of college name to be searched', 'course_id, college_name, course_abbr, course_name, course_desc', 2, NULL),
(141, 'Department.viewAll', 'view_department', 'Returns all the information of the department', 'department_json/view_department', 'access_code:the code given to access', 'dept_id, dept_name, dept_head, location', 3, NULL),
(142, 'Department.viewbyName', 'view_dept_by_name', 'Returns all the information of the department filtered by Department Name', 'department_json/view_dept_by_name', 'access_code:the code given to access, value: value of department name to be searched', 'dept_id, dept_name, dept_head, location', 3, NULL),
(143, 'Department.viewByHead', 'view_dept_by_head', 'Returns all the information of the department filtered by Department Head', 'department_json/view_dept_by_head', 'access_code:the code given to access, value: value of department head to be searched', 'dept_id, dept_name, dept_head, location', 3, NULL),
(144, 'Department.viewByLocation', 'view_dept_by_location', 'Returns all the information of the department filtered by Location', 'department_json/view_dept_by_location', 'access_code:the code given to access, value: value of location to be searched', 'dept_id, dept_name, dept_head, location', 3, NULL),
(145, 'Department.filterAll', 'view_dept_filterAll', 'Returns all the information of the department filtered by department name, head and location', 'department_json/view_dept_filterAll', 'access_code:the code given to access, value1: value of department name, value2: value of department head, value3: value of location', 'dept_id, dept_name, dept_head, location', 3, NULL),
(146, 'Subject.viewAll', 'view_subject', 'Returns all the information of a subject', 'subject_json/view_subject', 'access_code: the code given to access', 'subj_id, subj_name, subj_desc, units, lecture_hrs, lab_hrs, college_name', 4, NULL),
(147, 'Subject.viewByCollegeName', 'view_subject_by_collegeName', 'Returns all the information of a subject filtered by College Name', 'subject_json/view_subject_by_collegeName', 'access_code: the code given to access, value: value of college name to be searched. ', 'subj_id, subj_name, subj_desc, units, lecture_hrs, lab_hrs, college_name', 4, NULL),
(148, 'Subject.viewBySubjectName', 'view_subject_by_subjName', 'Returns all the information of a subject filtered by Subject Name', 'subject_json/view_subject_by_subjName', 'access_code: the code given to access, value: value of subject name to be searched.', 'subj_id, subj_name, subj_desc, units, lecture_hrs, lab_hrs, college_name', 4, NULL),
(149, 'Subject.viewBySubjectDesc', 'view_subject_by_subjDesc', 'Returns all the information of a subject filtered by Subject Description', 'subject_json/view_subject_by_subjDesc', 'access_code: the code given to access, value: value of subject description to be searched.', 'subj_id, subj_name, subj_desc, units, lecture_hrs, lab_hrs, college_name', 4, NULL),
(150, 'Subject.viewBySubjectUnits', 'view_subject_by_subjUnits', 'Returns all the information of a subject filtered by Subject Units', 'subject_json/view_subject_by_subjUnits', 'access_code: the code given to access, value: value of subject units to be searched.', 'subj_id, subj_name, subj_desc, units, lecture_hrs, lab_hrs, college_name', 4, NULL),
(151, 'Subject.viewBySubjectLabHrs', 'view_subject_by_subjLabHrs', 'Returns all the information of a subject filtered by Subject Lab Hours', 'subject_json/view_subject_by_subjLabHrs', 'access_code: the code given to access, value: value of subject lab hours to be searched', 'subj_id, subj_name, subj_desc, units, lecture_hrs, lab_hrs, college_name', 4, NULL),
(152, 'Subject.viewBySubjectLecHrs', 'view_subject_by_subjLecHrs', 'Returns all the information of a subject filtered by Subject Lec Hours', 'subject_json/view_subject_by_subjLecHrs', 'access_code: the code given to access, value: value of subject lec hours to be searched', 'subj_id, subj_name, subj_desc, units, lecture_hrs, lab_hrs, college_name', 4, NULL),
(153, 'Subject.FilterAll', 'view_subject_filterAll', 'Returns all the information of a subject filtered by Subject Lecture Hours', 'subject_json/view_subject_filterAll', 'access_code: the code given to access, value: value of all subject', 'subj_id, subj_name, subj_desc, units, lecture_hrs, lab_hrs, college_name', 4, NULL),
(154, 'SubjOffering.viewSubjectOffering', 'view_subj_schedule_offering', 'Returns all the information of a subject offering', 'subj_schedule_offering_json/view_subj_schedule_offering', 'access_code: the code given to access', 'offer_code, school_year, semester, day, time, duration, room_no, teacher_id, first_name, last_name, subj_id, subj_name,allocation, status', 4, NULL),
(155, 'SubjOffering.viewSubjectOfferingBySubjName', 'view_subj_schedule_offering_by_subjName', 'Returns all the information of a subject offering by Subject Name', 'subj_schedule_offering_json/view_subj_schedule_offering_by_subjName', 'access_code: the code given to access, value: value of subject name to be searched', 'offer_code, school_year, semester, day, time, duration, room_no, teacher_id, first_name, last_name, subj_id, subj_name,allocation, status', 4, NULL),
(156, 'SubjOffering.viewSubjectOfferingBytermSchoolY', 'view_subj_schedule_offering_by_termSchoolYear', 'Returns all the information of a subject offering by school year and term', 'subj_schedule_offering_json/view_subj_schedule_offering_by_termSchoolYear', 'access_code: the code given to access, value: value of year and semester to be searched', 'offer_code, school_year, semester, day, time, duration, room_no, teacher_id, first_name, last_name, subj_id, subj_name,allocation, status', 4, NULL),
(157, 'SubjOffering.viewSubjectOfferingBySchoolYear', 'view_subj_schedule_offering_by_schoolYear', 'Returns all the information of a subject offering by School Year', 'subj_schedule_offering_json/view_subj_schedule_offering_by_schoolYear', 'access_code: the code given to access, value: value of school year to be searched', 'offer_code, school_year, semester, day, time, duration, room_no, teacher_id, first_name, last_name, subj_id, subj_name,allocation, status', 4, NULL),
(158, 'SubjOffering.viewSubjectOfferingBySemester', 'view_subj_schedule_offering_by_semester', 'Returns all the information of a subject offering by Semester', 'subj_schedule_offering_json/view_subj_schedule_offering_by_semester', 'access_code: the code given to access, value: value of semester to be searched', 'offer_code, school_year, semester, day, time, duration, room_no, teacher_id, first_name, last_name, subj_id, subj_name,allocation, status', 4, NULL),
(159, 'SubjOffering.viewSubjectOfferingByDayTime', 'view_subj_schedule_offering_by_dayTime', 'Returns all the information of a subject offering by Day Time', 'subj_schedule_offering_json/view_subj_schedule_offering_by_dayTime', 'access_code: the code given to access, value: value of day and time to be searched', 'offer_code, school_year, semester, day, time, duration, room_no, teacher_id, first_name, last_name, subj_id, subj_name,allocation, status', 4, NULL),
(160, 'SubjOffering.viewSubjectOfferingByDay', 'view_subj_schedule_offering_by_day', 'Returns all the information of a subject offering by Day', 'subj_schedule_offering_json/view_subj_schedule_offering_by_day', 'access_code: the code given to access, value: value of day to be searched', 'offer_code, school_year, semester, day, time, duration, room_no, teacher_id, first_name, last_name, subj_id, subj_name,allocation, status', 4, NULL),
(161, 'SubjOffering.viewSubjectOfferingByTime', 'view_subj_schedule_offering_by_time', 'Returns all the information of a subject offering by Time', 'subj_schedule_offering_json/view_subj_schedule_offering_by_time', 'access_code: the code given to access, value: value of time to be searched', 'offer_code, school_year, semester, day, time, duration, room_no, teacher_id, first_name, last_name, subj_id, subj_name,allocation, status', 4, NULL),
(162, 'SubjOffering.viewSubjectOfferingBy/Duration', 'view_subj_schedule_offering_by_duration', 'Returns all the information of a subject offering by Duration', 'subj_schedule_offering_json/view_subj_schedule_offering_by_duration', 'access_code: the code given to access, value: value of duration to be searched', 'offer_code, school_year, semester, day, time, duration, room_no, teacher_id, first_name, last_name, subj_id, subj_name,allocation, status', 4, NULL),
(163, 'SubjOffering.viewSubjectOfferingByroomNo', 'view_subj_schedule_offering_by_roomNo', 'Returns all the information of a subject offering by room number', 'subj_schedule_offering_json/view_subj_schedule_offering_by_roomNo', 'access_code: the code given to access, value: value of room number to be searched', 'offer_code, school_year, semester, day, time, duration, room_no, teacher_id, first_name, last_name, subj_id, subj_name,allocation, status', 4, NULL),
(164, 'SubjOffering.viewSubjectOfferingByAllocation', 'view_subj_schedule_offering_by_allocation', 'Returns all the information of a subject offering by Allocation', 'subj_schedule_offering_json/view_subj_schedule_offering_by_allocation', 'access_code: the code given to access, value: value of allocation to be searched', 'offer_code, school_year, semester, day, time, duration, room_no, teacher_id, first_name, last_name, subj_id, subj_name,allocation, status', 4, NULL),
(165, 'SubjOffering.viewSubjectOfferingByStatus', 'view_subj_schedule_offering_by_status', 'Returns all the information of a subject offering by Status', 'subj_schedule_offering_json/view_subj_schedule_offering_by_status', 'access_code: the code given to access, value: value of status to be searched', 'offer_code, school_year, semester, day, time, duration, room_no, teacher_id, first_name, last_name, subj_id, subj_name,allocation, status', 4, NULL),
(166, 'SubjOffering.viewSubjectOfferingFilterAll', 'view_subj_schedule_offering_filterall', 'Returns all the information of a subject offering by offer code, by school year, by semester, by day, by time, by duration, by room_no, by allocation, by status, by first name, by last name', 'subj_schedule_offering_json/view_subj_schedule_offering_filterall', 'access_code: the code given to access, value: value of offer_code, school_year, semester, day, time, duration, room_no, teacher_id, first_name, last_name, subj_id, subj_name,allocation, status to be searched', 'offer_code, school_year, semester, day, time, duration, room_no, teacher_id, first_name, last_name, subj_id, subj_name,allocation, status', 4, NULL),
(167, 'SubjOffering.viewSubjectOfferingbyLastName', 'view_subj_schedule_offering_by_lastName', 'Returns all the information of a subject offering by last name of teacher', 'subj_schedule_offering_json/view_subj_schedule_offering_by_lastName', 'access_code: the code given to access, value: value of last name of teacher to be searched', 'offer_code, school_year, semester, day, time, duration, room_no, teacher_id, first_name, last_name, subj_id, subj_name,allocation, status', 4, NULL),
(168, 'SubjOffering.viewSubjectOfferingFirstName', 'view_subj_schedule_offering_by_firstName', 'Returns all the information of a subject offering by first name of teacher', 'subj_schedule_offering_json/view_subj_schedule_offering_by_firstName', 'access_code: the code given to access, value: value of first name of teacher to be searched', 'offer_code, school_year, semester, day, time, duration, room_no, teacher_id, first_name, last_name, subj_id, subj_name,allocation, status', 4, NULL),
(169, 'Personnel.viewAll', 'view_personnel', 'Returns all the information of the personnel', 'personnel_json/view_personnel', 'access_code: the code to access function.', 'person_id, last_name, first_name, middle_name, gender, birthdate, address, religion, personnel_id, sss, tin, position, dept_id, dept_name', 9, NULL),
(170, 'Personnel.viewBySSS', 'view_personnel_by_sss', 'Returns all the information of the personnel filtered by sss.', 'personnel_json/view_personnel_by_sss', 'access_code: the code to access function, value: value of SSS to be searched. ', 'person_id, last_name, first_name, middle_name, gender, birthdate, address, religion, personnel_id, sss, tin, position, dept_id, dept_name', 9, NULL),
(172, 'Personnel.viewByTIN', 'view_personnel_by_tin', 'Returns all the information of the personnel filtered by TIN', 'personnel_json/view_personnel_by_tin', 'access_code: the code to access function, value: value of TIN to be searched. ', 'person_id, last_name, first_name, middle_name, gender, birthdate, address, religion, personnel_id, sss, tin, position, dept_id, dept_name', 9, NULL),
(173, 'Personnel.viewByPosition', 'view_personnel_by_position', 'Returns all the information of the personnel filtered by position', 'personnel_json/view_personnel_by_position', 'access_code: the code to access function, value: value of position to be searched. ', 'person_id, last_name, first_name, middle_name, gender, birthdate, address, religion, personnel_id, sss, tin, position, dept_id, dept_name', 9, NULL),
(174, 'Personnel.viewByLastName', 'view_personnel_by_lastName', 'Returns all the information of the personnel filtered by last name', 'personnel_json/view_personnel_by_lastName', 'access_code: the code to access function, value: value of last name to be searched. ', 'person_id, last_name, first_name, middle_name, gender, birthdate, address, religion, personnel_id, sss, tin, position, dept_id, dept_name', 9, NULL),
(175, 'Personnel.viewByFirstName', 'view_personnel_by_firstName', 'Returns all the information of the personnel filtered by first name', 'personnel_json/view_personnel_by_firstName', 'access_code: the code to access function, value: value of first name to be searched. ', 'person_id, last_name, first_name, middle_name, gender, birthdate, address, religion, personnel_id, sss, tin, position, dept_id, dept_name', 9, NULL),
(176, 'Personnel.viewByFullName', 'view_personnel_by_fullName', 'Returns all the information of the personnel filtered by full name (first name last name)', 'personnel_json/view_personnel_by_fullName', 'access_code: the code to access function, value: value of first name and last name to be searched. ', 'person_id, last_name, first_name, middle_name, gender, birthdate, address, religion, personnel_id, sss, tin, position, dept_id, dept_name', 9, NULL),
(177, 'Personnel.viewByDeptName', 'view_personnel_by_deptName', 'Returns all the information of the personnel filtered by department name', 'personnel_json/view_personnel_by_deptName', 'access_code: the code to access function, value: value of department name to be searched. ', 'person_id, last_name, first_name, middle_name, gender, birthdate, address, religion, personnel_id, sss, tin, position, dept_id, dept_name', 9, NULL),
(178, 'Personnel.filterAll', 'view_personnel_filterAll', 'Returns all the information of the personnel filtered by last name, first name, sss, tin, department, position', 'personnel_json/view_personnel_filterAll', 'access_code: the code to access function, value1: value of last name, value2: value of first name, value3: value of sss, value4: value of tin, value5: value of department, value6: value of position', 'person_id, last_name, first_name, middle_name, gender, birthdate, address, religion, personnel_id, sss, tin, position, dept_id, dept_name', 9, NULL),
(179, 'Teacher.viewAll', 'view_teacher', 'returns all the information of a teacher', 'teacher_json/view_teacher', 'access_code: the code given to access the function.', 'person_id, last_name, first_name,middle_name, gender, birthdate, address, religion, teacher_id, sss, tin, college_id, college_name', 8, NULL),
(180, 'Teacher.viewByLastName', 'view_teacher_by_lastName', 'returns all the information of a teacher filtered by last name', 'teacher_json/view_teacher_by_lastName', 'access_code: the code given to access the function\r\nvalue : value of last name', 'person_id, last_name, first_name,middle_name, gender, birthdate, address, religion, teacher_id, sss, tin, college_id, college_name', 8, NULL),
(181, 'Teacher.viewByFirstName', 'view_teacher_by_firstName', 'returns all the information of the teacher filtered by first name', 'teacher_json/view_teacher_by_firstName', 'access_code: the code given to access the function, value : value of first name', 'person_id, last_name, first_name,middle_name, gender, birthdate, address, religion, teacher_id, sss, tin, college_id, college_name', 8, NULL),
(182, 'Teacher.viewByFullName', 'view_teacher_by_fullName', 'Returns all the information of the teacher filtered by full name', 'teacher_json/view_teacher_by_fullName', 'access_code: the code given to access the function, value : value of first name and last name', 'person_id, last_name, first_name,middle_name, gender, birthdate, address, religion, teacher_id, sss, tin, college_id, college_name', 8, NULL),
(183, 'Teacher.viewBySSS', 'view_teacher_by_sss', 'Returns all the information of the teacher filtered by SSS.', 'teacher_json/view_teacher_by_sss', 'access_code: the code given to access the function, value : value of SSS', 'person_id, last_name, first_name,middle_name, gender, birthdate, address, religion, teacher_id, sss, tin, college_id, college_name', 8, NULL),
(184, 'Teacher.viewByTIN', 'view_teacher_by_tin', 'Returns all the information of the teacher filtered by TIN.', 'teacher_json/view_teacher_by_tin', 'access_code: the code given to access the function, value : value of TIN', 'person_id, last_name, first_name,middle_name, gender, birthdate, address, religion, teacher_id, sss, tin, college_id, college_name', 8, NULL),
(185, 'Teacher.viewByCollege', 'view_teacher_by_college', 'Returns all the information of the teacher by College', 'teacher_json/view_teacher_by_college', 'access_code: the code given to access the function, value : value of College Name', 'person_id, last_name, first_name,middle_name, gender, birthdate, address, religion, teacher_id, sss, tin, college_id, college_name', 8, NULL),
(186, 'Teacher.filterAll', 'view_teacher_filterAll', 'Returns all the information of the teacher filtered by last name, first name, sss, tin and college name', 'teacher_json/view_teacher_filterAll', 'access_code: the code given to access the function, \r\nvalue1 : value of Last Name,\r\nvalue2 : value of First Name,\r\nvalue3 : value of SSS,\r\nvalue4 : value of TIN,\r\nvalue5 : value of College Name', 'person_id, last_name, first_name,middle_name, gender, birthdate, address, religion, teacher_id, sss, tin, college_id, college_name', 8, NULL),
(187, 'Teacher.subjHandledByName', 'view_teacher_subjHandled_by_name', 'returns all the subject of the teacher filtered by teacher name', 'teacher_json/view_teacher_subjHandled_by_name', 'access_code: the code given to access the function, \r\nvalue1 : Last Name,\r\nvalue2 : First Name', 'person_id, last_name, first_name,middle_name, gender, birthdate, address, religion, teacher_id, sss, tin, college_id, college_name', 8, NULL),
(188, 'Teacher.subjHandledByName&Term', 'view_teacher_subjHandled_by_name_term', ' returns all the subject of the teacher filtered by teacher name and term (school year and semester)', 'teacher_json/view_teacher_subjHandled_by_name_term', 'access_code: the code given to access the function, \r\nvalue1 : Last Name, \r\nvalue2 : First Name,\r\nvalue3 : School year,\r\nvalue4 : Semester', 'person_id, last_name, first_name,middle_name, gender, birthdate, address, religion, teacher_id, sss, tin, college_id, college_name', 8, NULL),
(189, 'Teacher.studentsUnder', 'view_teacher_studentsUnder', 'returns the name of the student, course and year under the specified teacher.', 'teacher_json/view_teacher_studentsUnder', 'access_code: the code given to access the function, value1 : Last Name, value2 : First Name', 'person_id, last_name, first_name,middle_name, gender, birthdate, address, religion, teacher_id, sss, tin, college_id, college_name', 8, NULL),
(190, 'Student.viewAll', 'get_students', 'Returns all the information of all the student in USJR', 'student_json/get_students', 'access_code : the code given to access', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl', 7, NULL),
(191, 'Student.viewStudentbyID', 'get_students_ID', 'Return information of student filtered by student ID', 'student_json/get_students_ID', 'access_code : the code given to access\r\nvalue1: STUDENT ID', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl', 7, NULL),
(192, 'Student.viewStudentbyFname', 'get_students_fName', 'Return information of student filtered by first name', 'student_json/get_students_fName', 'access_code : the code given to access \r\nvalue1: FIRST NAME', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl', 7, NULL),
(193, 'Student.viewStudentbyLname', 'get_students_lName', 'Return information of student filtered by last name', 'student_json/get_students_lName', 'access_code : the code given to access \r\nvalue1: LAST NAME', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl', 7, NULL),
(194, 'Student.viewStudentbyGender', 'get_students_Gender', 'Return information of student filtered by gender', 'student_json/get_students_Gender', 'access_code : the code given to access \r\nvalue1: GENDER', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl', 7, NULL),
(195, 'Student.viewStudentbyReligion', 'get_students_Religion', 'Return information of student filtered by religion', 'student_json/get_students_Religion', 'access_code : the code given to access \r\nvalue1: RELIGION', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl', 7, NULL),
(196, 'Student.viewStudentbyCourse', 'get_students_Course', 'Return information of student filtered by course', 'student_json/get_students_Course', 'access_code : the code given to access \r\nvalue1: COURSE', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl', 7, NULL),
(197, 'Student.viewStudentbyYear', 'get_students_Year', 'Return information of student filtered by year', 'student_json/get_students_Year', 'access_code : the code given to access \r\nvalue1: YEAR', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl', 7, NULL),
(198, 'Student.viewStudentbyMajor', 'get_students_Major', 'Return information of student filtered by major', 'student_json/get_students_Major', 'access_code : the code given to access \r\nvalue1: YEAR', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl', 7, NULL),
(199, 'Student.viewStudentbyYearAndCourse', 'get_students_YearAndCourse', 'Return information of student filtered by year and course', 'student_json/get_students_YearAndCourse', 'access_code : the code given to access \r\nvalue1: YEAR\r\nvalue2: COURSE', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl', 7, NULL),
(200, 'Student.viewStudentbyCourseAndMajor', 'get_students_CourseAndMajor', 'Return information of student filtered by course and major', 'student_json/get_students_CourseAndMajor', 'access_code : the code given to access value1: COURSE value2: MAJOR', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl', 7, NULL),
(201, 'Student.viewStudentbyNameCourseYear', 'get_students_NameAndCourseYear', 'Return information of student filtered by name, course and year', 'student_json/get_students_NameAndCourseYear', 'access_code : the code given to access value1: COURSE value2: YEAR value3: FIRST NAME value4: LAST NAME', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl', 7, NULL),
(202, 'Student.viewStudentbyNameYear', 'get_students_NameYear', 'Return information of student filtered by name and year', 'student_json/get_students_NameYear', 'access_code : the code given to access value1: YEAR value2: FIRST NAME value3: LAST NAME', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl', 7, NULL),
(203, 'Student.viewStudentbyNameCourse', 'get_students_NameCourse', 'Return information of student filtered by name and year', 'student_json/get_students_NameCourse', 'access_code : the code given to access value1: COURSE value2: FIRST NAME value3: LAST NAME', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl', 7, NULL),
(204, 'Student.viewStudentbyNameMajor', 'get_students_NameMajor', 'Return information of student filtered by name and major', 'student_json/get_students_NameMajor', 'access_code : the code given to access value1: MAJOR value2: FIRST NAME value3: LAST NAME', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl', 7, NULL),
(205, 'Student.viewStudentbyNameSubjects', 'get_students_NameSubjects', 'Return information of student filtered by name and subjects', 'student_json/get_students_NameSubjects', 'access_code : the code given to access value1: SUBJECTS value2: FIRST NAME value3: LAST NAME', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl', 7, NULL),
(206, 'Student.viewStudentbyNameAddress', 'get_students_NameAndAddress', 'Return information of student filtered by name and description', 'student_json/get_students_NameAndAddress', 'access_code : the code given to access value1: ADDRESS value2: FIRST NAME value3: LAST NAME', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl', 7, NULL),
(207, 'Student.viewStudentbyNameAndGrades', 'get_students_NameAndGrades', 'Return information of student filtered by name and grades', 'student_json/get_students_NameAndGrades', 'access_code : the code given to access value1: FIRST NAME value2: LAST NAME', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl, prelim, midterm, finals', 7, NULL),
(208, 'Student.viewStudentbyNameAndPrelimGrades', 'get_students_NameAndPrelimGrades', 'Return information of student filtered by name and prelim grade', 'student_json/get_students_NameAndPrelimGrades', 'access_code : the code given to access value3: PRELIM GRADE\r\nvalue1: FIRST NAME value2: LAST NAME', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl, prelim, midterm, finals', 7, NULL),
(209, 'Student.viewStudentbyNameAndMidtermGrades', 'get_students_NameAndMidTermGrades', 'Return information of student filtered by name and midterm grade', 'student_json/get_students_NameAndMidTermGrades', 'access_code : the code given to access value3: MIDTERM GRADE value1: FIRST NAME value2: LAST NAME', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl, prelim, midterm, finals', 7, NULL),
(210, 'Student.viewStudentbyNameAndFinalGrades', 'get_students_NameAndFinalGrades', 'Return information of student filtered by name and final grade', 'student_json/get_students_NameAndFinalGrades', 'access_code : the code given to access value3: FINAL GRADE value1: FIRST NAME value2: LAST NAME', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl, prelim, midterm, finals', 7, NULL),
(211, 'Student.subjEnrolled', 'list_of_subjEnrolled_by_student', 'Returns all the subject enrolled by the student', 'studentRelated_json/list_of_subjEnrolled_by_student', 'access:code, last name, first name', 'student_no, last_name, first_name, course_abbr, year_lvl, subj_code, subj_name, offer_code, school_year, semester', 7, NULL),
(212, 'Student.subjEnrolledBySem', 'list_of_subjEnrolled_by_student_bysem', 'Returns all the subject enrolled by the student', 'studentRelated_json/list_of_subjEnrolled_by_student_bysem', 'access:code, last name, first name, school year, semester', 'student_no, last_name, first_name, course_abbr, year_lvl, subj_code, subj_name, offer_code, school_year, semester', 7, NULL),
(213, 'Student.subjEnrolledSchedule', 'list_of_subjEnrolled_by_student_with_schedule', 'Returns all the subject enrolled by the student', 'studentRelated_json/list_of_subjEnrolled_by_student_with_schedule', 'access code, last name, first name', 'student_no, last_name, first_name, course_abbr, year_lvl, subj_code, subj_name, offer_code, day, time, room_no, school_year, semester', 7, NULL),
(214, 'Student.subjEnrolledSchedulePerSem', 'list_of_subjEnrolled_by_student_with_schedule_sem', 'Returns all the subject enrolled by the student per semester', 'studentRelated_json/list_of_subjEnrolled_by_student_with_schedule_sem', 'access_code, last_name, first_name, school_year, semester', 'student_no, last_name, first_name, course_abbr, year_lvl, subj_code, subj_name, offer_code, day, time, room_no, school_year, semester', 7, NULL),
(215, 'Student.viewStudentByCourseYearAndMajor', 'get_students_CourseAndMajor', 'Return information of student filtered by course, year and major.', 'student_json/get_students_CourseAndMajor', 'access_code : the code given to access value1: COURSE NAME value2: FIRST NAME value3: LAST NAME', 'student_id, last_name, first_name, middle_name, gender, birthdate, address, religion, course_name, major, year_lvl', 7, NULL),
(216, 'Teacher.studentsUnderWhoFailed', 'view_teacher_studentsUnderWhoFailed', 'Returns the names of the student who was failed by a teacher', 'teacher_json/view_teacher_studentsUnderWhoFailed', 'access_code, last_name, first_name', 'last_name, first_name, course_name, year_lvl', 8, NULL),
(217, 'Subjects.studentList', 'list_of_students_enrolled_in_subj', 'returns all the student enrolled in a specified subject', 'studentRelated_json/list_of_students_enrolled_in_subj', 'access_code, value(subject_name)', 'student_id, last_name, first_name, course_abbr, year_lvl, major', 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE IF NOT EXISTS `grades` (
  `student_no` int(11) NOT NULL,
  `school_year` varchar(15) NOT NULL,
  `semester` varchar(15) NOT NULL,
  `subj_code` int(11) NOT NULL,
  `subj_name` varchar(45) NOT NULL,
  `prelim` double DEFAULT '0',
  `midterm` double DEFAULT '0',
  `finals` double DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`student_no`, `school_year`, `semester`, `subj_code`, `subj_name`, `prelim`, `midterm`, `finals`) VALUES
(2006007993, '2011-2012', '1', 3002, 'IT 211', 2.5, 2.3, 2),
(2006007993, '2011-2012', '1', 3003, 'IT 212', 2, 3, 5),
(2006007993, '2011-2012', '1', 3004, 'IT 213', 1.6, 1.5, 1.7),
(2006009003, '2010-2011', '1', 1001, 'ENGLISH 1', 1.6, 1.4, 1.3),
(2006009003, '2010-2011', '1', 1002, 'GUIDANCE 1', 1.7, 1.5, 1.5),
(2006009003, '2010-2011', '1', 1003, 'IT 110', 1.8, 1.6, 1.5),
(2006009003, '2010-2011', '1', 1004, 'IT 111', 1.9, 1.7, 1.5),
(2006009003, '2010-2011', '1', 1005, 'KB 1', 1.8, 1.6, 1.3),
(2006009003, '2010-2011', '1', 1006, 'MATH 1', 1.4, 1.3, 1.4),
(2006009003, '2010-2011', '2', 2003, 'IT 120', 1.6, 1.5, 1.7),
(2006009003, '2010-2011', '2', 2004, 'IT 121', 1.5, 1.7, 1.5),
(2006009003, '2010-2011', '2', 2005, 'IT 122', 3, 2.8, 5),
(2006009003, '2012-2013', '2', 4000, 'IT 221', 1.5, 1.7, 1.5),
(2006009003, '2012-2013', '2', 4001, 'IT 222', 3, 2.8, 5),
(2006009003, '2012-2013', '2', 4002, 'IT 223', 2.5, 2.3, 2),
(2006009003, '2013-2014', '1', 6006, 'IT 321', 1.5, 1.7, 1.4),
(2006009003, '2013-2014', '1', 6007, 'IT 323', 1.6, 1.5, 1.7),
(2006009003, '2013-2014', '1', 6008, 'IT 324', 1.5, 1.7, 1.5),
(2006009003, '2013-2014', '1', 6009, 'IT 322', 2.5, 2.7, 5),
(2006009003, '2013-2014', '2', 7000, 'IT 331', 1.5, 1.7, 1.4),
(2006009003, '2013-2014', '2', 7001, 'IT 332', 1.6, 1.5, 1.7),
(2006009003, '2013-2014', '2', 7002, 'FE 1', 1.5, 1.7, 1.5),
(2010006085, '2010-2011', '1', 1007, 'MATH 2', 1.4, 1.3, 1.5),
(2010006085, '2010-2011', '1', 1008, 'PE 1', 1.2, 1.4, 1.7),
(2010006085, '2010-2011', '1', 1009, 'REED 1', 1.5, 1.2, 1.8);

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE IF NOT EXISTS `person` (
`person_id` int(11) NOT NULL,
  `last_name` varchar(15) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `middle_name` varchar(15) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `birthdate` varchar(45) NOT NULL,
  `address` text NOT NULL,
  `religion` varchar(45) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=501 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`person_id`, `last_name`, `first_name`, `middle_name`, `gender`, `birthdate`, `address`, `religion`) VALUES
(1, 'GABISAN', 'ANECITA', 'M', 'F', '09/22/1993', 'CEBU CITY', 'ROMAN CATHOLIC'),
(2, 'LOPERNES', 'ARCHIE', 'P', 'M', '06/27/1994', 'TALISAY CITY', 'ROMAN CATHOLIC'),
(3, 'OEN', 'HANNAH MICHELLE', 'G', 'F', '06/08/1993', 'CITY OF NAGA', 'NONE'),
(4, 'KONEKTOR', 'PROJECT', 'T', 'F', '11/14/2013', 'CEBU CITY', 'ROMAN CATHOLIC'),
(140, 'ABARQUEZ', 'JETHRO', 'Q', 'M', '02/20/1993', 'CEBU CITY', 'CATHOLIC'),
(141, 'ALERTA', 'AIRA ELAINE', 'H', 'F', '02/21/1993', 'CEBU CITY', 'CATHOLIC'),
(142, 'ARMERO', 'RALPH RAMER', 'Y', 'M', '02/22/1993', 'CEBU CITY', 'CATHOLIC'),
(143, 'BAYANI', 'THYLIS', 'T', 'F', '02/23/1993', 'CEBU CITY', 'CATHOLIC'),
(144, 'CAINTAPAN', 'CINDY PEARL', 'S', 'F', '02/24/1993', 'CEBU CITY', 'CATHOLIC'),
(145, 'CALIPAY', 'CATHERINE', 'A', 'F', '02/25/1993', 'CEBU CITY', 'CATHOLIC'),
(146, 'CANONIGO', 'MA. THERESA', 'T', 'F', '02/26/1993', 'CEBU CITY', 'CATHOLIC'),
(147, 'CANONIGO', 'BRADFORD', 'C', 'M', '02/27/1993', 'CEBU CITY', 'CATHOLIC'),
(148, 'CAYA', 'IRES', 'F', 'F', '02/28/1993', 'CEBU CITY', 'CATHOLIC'),
(149, 'CONJE', 'JANN', 'W', 'M', '03/01/1993', 'CEBU CITY', 'CATHOLIC'),
(150, 'DACULAN', 'VAN LAURENCE', 'Q', 'M', '03/02/1993', 'CEBU CITY', 'CATHOLIC'),
(151, 'DE LA GUARDIA', 'JAKE', 'T', 'M', '03/03/1993', 'CEBU CITY', 'CATHOLIC'),
(152, 'DE LA PIEDRA', 'ADRIAN', 'Y', 'M', '03/04/1993', 'CEBU CITY', 'CATHOLIC'),
(153, 'DE LA TORRE', 'JED', 'T', 'M', '03/05/1993', 'CEBU CITY', 'CATHOLIC'),
(154, 'DEIPARINE', 'CARL JOSHUA', 'V', 'M', '03/06/1993', 'CEBU CITY', 'CATHOLIC'),
(155, 'DEIPARINE', 'BIANCA', 'E', 'F', '03/07/1993', 'CEBU CITY', 'CATHOLIC'),
(156, 'ECONAS', 'CLINT JAY', 'Z', 'M', '03/08/1993', 'CEBU CITY', 'CATHOLIC'),
(157, 'EMIA', 'ALTAIR REY', 'A', 'M', '03/09/1993', 'CEBU CITY', 'CATHOLIC'),
(158, 'GABISAN', 'ANECITA', 'MOSQUERA', 'F', '09/22/1993', 'CEBU CITY', 'CATHOLIC'),
(159, 'GALLOSA', 'MELISSA', 'Q', 'F', '03/11/1993', 'CEBU CITY', 'CATHOLIC'),
(160, 'GAMBOA', 'NINO', 'J', 'M', '03/12/1993', 'CEBU CITY', 'CATHOLIC'),
(161, 'GENDRANA', 'KIMBERLIGH', 'S', 'F', '03/13/1993', 'CEBU CITY', 'CATHOLIC'),
(162, 'GO', 'DANIEL ANGELO', 'BALLESTEROS', 'M', '03/14/1993', 'CEBU CITY', 'CATHOLIC'),
(163, 'LANSANG', 'GD ADRIAN', 'P', 'M', '03/15/1993', 'CEBU CITY', 'CATHOLIC'),
(164, 'LOGARTA', 'ARCHIE', 'B', 'M', '03/16/1993', 'CEBU CITY', 'CATHOLIC'),
(165, 'LOPERNES', 'ARCHIE', 'PACALDO', 'M', '06/27/1994', 'CEBU CITY', 'CATHOLIC'),
(166, 'MACASAOL', 'SHARON', 'H', 'F', '03/18/1993', 'CEBU CITY', 'CATHOLIC'),
(167, 'MANILONG', 'FAITH ANNE', 'W', 'F', '03/19/1993', 'CEBU CITY', 'CATHOLIC'),
(168, 'MARCELLANA', 'SHELDON', 'J', 'M', '03/20/1993', 'CEBU CITY', 'CATHOLIC'),
(169, 'MATUGAS', 'FLOYD EUGENE', 'O', 'M', '03/21/1993', 'CEBU CITY', 'CATHOLIC'),
(170, 'OBREGON', 'REYNALDO JR.', 'F', 'M', '03/22/1993', 'CEBU CITY', 'CATHOLIC'),
(171, 'OEN', 'HANNAH MICHELLE', 'GABATO', 'F', '06/08/1993', 'CEBU CITY', 'CATHOLIC'),
(172, 'PATERES', 'ROMNEL', 'P', 'M', '03/24/1993', 'CEBU CITY', 'CATHOLIC'),
(173, 'PEPITO', 'JULIUS', 'E', 'M', '03/25/1993', 'CEBU CITY', 'CATHOLIC'),
(174, 'QUENUNES', 'FAMELA', 'G', 'F', '03/26/1993', 'CEBU CITY', 'CATHOLIC'),
(175, 'RASCO', 'HAROLD', 'R', 'M', '03/27/1993', 'CEBU CITY', 'CATHOLIC'),
(176, 'ROSALES', 'JHONNEL', 'D', 'M', '03/28/1993', 'CEBU CITY', 'CATHOLIC'),
(177, 'SENANIN', 'LEOMAR', 'R', 'M', '03/29/1993', 'CEBU CITY', 'CATHOLIC'),
(178, 'SIACOR', 'LORD PATRICK', 'T', 'M', '03/30/1993', 'CEBU CITY', 'CATHOLIC'),
(179, 'SILVA', 'KAREN', 'T', 'F', '03/31/1993', 'CEBU CITY', 'CATHOLIC'),
(180, 'VILLABAN', 'JESSA', 'S', 'F', '04/01/1993', 'CEBU CITY', 'CATHOLIC'),
(181, 'YLAYA', 'RAYMUND VINCENT', 'Y', 'M', '04/02/1993', 'CEBU CITY', 'CATHOLIC'),
(182, 'BANDALAN', 'RODERICK', 'A', 'M', '01/23/1987', 'CEBU CITY', 'CATHOLIC'),
(183, 'ABELLO', 'GENE', 'P', 'M', '01/24/1987', 'CEBU CITY', 'CATHOLIC'),
(184, 'MADRID', 'MAC ALJUN', 'D', 'M', '01/25/1987', 'CEBU CITY', 'CATHOLIC'),
(185, 'ANTONINO', 'MARK CEDRICK', 'I', 'M', '01/26/1987', 'CEBU CITY', 'CATHOLIC'),
(186, 'GUDIO', 'JEOFFREY', 'C', 'M', '01/27/1987', 'CEBU CITY', 'CATHOLIC'),
(187, 'MIRO', 'LORNA', 'D', 'F', '01/28/1987', 'CEBU CITY', 'CATHOLIC'),
(188, 'PETRALBA', 'JOSEPHINE', 'E', 'F', '01/29/1987', 'CEBU CITY', 'CATHOLIC'),
(189, 'TEJANA', 'CARMEL', 'M', 'F', '01/30/1987', 'CEBU CITY', 'CATHOLIC'),
(190, 'MAHILUM', 'MARISA', 'M', 'F', '01/31/1987', 'CEBU CITY', 'CATHOLIC'),
(191, 'CUIZON', 'JOVELYN', 'C', 'F', '02/01/1987', 'CEBU CITY', 'CATHOLIC'),
(192, 'DUYAGUIT', 'ARNEL', 'G', 'M', '02/02/1987', 'CEBU CITY', 'CATHOLIC'),
(193, 'SALARES', 'ROY', 'M', 'M', '02/03/1987', 'CEBU CITY', 'CATHOLIC'),
(194, 'ROCAMORA', 'MARLON', 'T', 'M', '02/04/1987', 'CEBU CITY', 'CATHOLIC'),
(195, 'REYES', 'JOAN LOU MARIE', 'M', 'F', '02/05/1987', 'CEBU CITY', 'CATHOLIC'),
(196, 'GOK-ONG ', 'ELDRIN PAUL', 'R', 'M', '04/02/1978', 'CEBU CITY', 'CATHOLIC'),
(197, 'GOLBE ', 'KETTE', 'A', 'M', '04/03/1978', 'CEBU CITY', 'CATHOLIC'),
(198, 'GOMEZ ', 'JEANNELY FAITH', 'V', 'M', '04/04/1978', 'CEBU CITY', 'CATHOLIC'),
(199, 'GOMEZ ', 'ROLLY JOE', 'U', 'M', '04/05/1978', 'TALISAY CITY', 'CATHOLIC'),
(200, 'GONO ', 'JUNRIEL', 'P', 'M', '04/06/1978', 'TALISAY CITY', 'CATHOLIC'),
(201, 'INOCANDO ', 'JAMES', 'P', 'M', '04/29/1978', 'TALISAY CITY', 'CATHOLIC'),
(202, 'INOFERIO ', 'CONCORDIO JR.', 'E', 'M', '04/30/1978', 'TALISAY CITY', 'CATHOLIC'),
(203, 'INTES ', 'KENNETH', 'P', 'M', '05/01/1978', 'TALISAY CITY', 'CATHOLIC'),
(204, 'INTUD ', 'CHEZ ANDREW', 'N', 'M', '05/02/1978', 'TALISAY CITY', 'CATHOLIC'),
(205, 'ITO ', 'RYOMA', '', 'M', '05/03/1978', 'TALISAY CITY', 'CATHOLIC'),
(206, 'JABADAN ', 'JONEL', 'A', 'M', '05/04/1978', 'TALISAY CITY', 'CATHOLIC'),
(207, 'LIMORAN ', 'ALEXANDRA ', 'D', 'M', '07/01/1978', 'TALISAY CITY', 'CATHOLIC'),
(208, 'PELIGRINO', 'SHIELA', 'A', 'F', '02/12/1987', 'CEBU CITY', 'CATHOLIC'),
(209, 'BERONQUE', 'EDGAR', 'B', 'M', '02/25/1987', 'CEBU CITY', 'CATHOLIC'),
(210, 'ROSINTO', 'HERMENIGILDA', 'C', 'F', '03/15/1987', 'CEBU CITY', 'CATHOLIC'),
(211, 'TAPANAN', 'MONTANO', 'D', 'M', '03/18/1987', 'CEBU CITY', 'CATHOLIC'),
(212, 'ENAD', 'EDMOND', 'E', 'M', '03/22/1987', 'CEBU CITY', 'CATHOLIC'),
(213, 'BANDALAN', 'RODERICK', 'A', 'M', '1/23/1987', 'CEBU CITY', 'CATHOLIC'),
(214, 'ABELLO', 'GENE', 'P', 'M', '1/24/1987', 'CEBU CITY', 'CATHOLIC'),
(215, 'MADRID', 'MAC AILJUN', 'D', 'M', '1/25/1987', 'CEBU CITY', 'CATHOLIC'),
(216, 'ANTONINO', 'MARK CEDRICK', 'I', 'M', '1/26/1987', 'CEBU CITY', 'CATHOLIC'),
(217, 'GUDIO', 'JEOFFREY', 'C', 'M', '1/27/1987', 'CEBU CITY', 'CATHOLIC'),
(218, 'MIRO', 'LORNA', 'D', 'F', '1/28/1987', 'CEBU CITY', 'CATHOLIC'),
(219, 'PETRALBA', 'JOSEPHINE', 'E', 'F', '1/29/1987', 'CEBU CITY', 'CATHOLIC'),
(220, 'TEJANA', 'CARMEL', 'M', 'F', '1/30/1987', 'CEBU CITY', 'CATHOLIC'),
(221, 'MAHILUM', 'MARISA', 'M', 'F', '1/31/1987', 'CEBU CITY', 'CATHOLIC'),
(222, 'CUIZON', 'JOVELYN', 'C', 'F', '2/1/1987', 'CEBU CITY', 'CATHOLIC'),
(223, 'DUYAGUIT', 'ARNEL', 'G', 'M', '2/2/1987', 'CEBU CITY', 'CATHOLIC'),
(224, 'SALARES', 'ROY', 'M', 'M', '2/3/1987', 'CEBU CITY', 'CATHOLIC'),
(225, 'ROCAMORA', 'MARLON', 'T', 'M', '2/4/1987', 'CEBU CITY', 'CATHOLIC'),
(226, 'REYES', 'JOAN LOU MARIE', 'M', 'F', '2/5/1987', 'CEBU CITY', 'CATHOLIC'),
(227, 'PELIGRINO', 'SHIELA', 'A', 'F', '2/12/1987', 'CEBU CITY', 'CATHOLIC'),
(228, 'BERONQUE', 'EDGAR', 'B', 'M', '2/25/1987', 'CEBU CITY', 'CATHOLIC'),
(229, 'ROSINTO', 'HERMENIGILDA', 'C', 'F', '3/15/1987', 'CEBU CITY', 'CATHOLIC'),
(230, 'TAPANAN', 'MONTANO', 'D', 'M', '3/18/1987', 'CEBU CITY', 'CATHOLIC'),
(231, 'ENAD', 'EDMOND', 'E', 'M', '3/22/1987', 'CEBU CITY', 'CATHOLIC'),
(500, 'Pable', 'Glendom', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `personnel`
--

CREATE TABLE IF NOT EXISTS `personnel` (
  `personnel_id` int(11) NOT NULL,
  `sss` int(11) NOT NULL,
  `tin` int(11) NOT NULL,
  `position` varchar(45) DEFAULT NULL,
  `dept_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `personnel`
--

INSERT INTO `personnel` (`personnel_id`, `sss`, `tin`, `position`, `dept_id`, `person_id`) VALUES
(60001, 231145, 200051, 'ASSISTANT HEAD', 1011, 196),
(60002, 231146, 200052, 'ASSISTANT HEAD', 1012, 197),
(60003, 231147, 200053, 'ASSISTANT HEAD', 1013, 198),
(60004, 231148, 200054, 'ASSISTANT HEAD', 1014, 199),
(60005, 231149, 200055, 'ASSISTANT HEAD', 1015, 200),
(60006, 231172, 200056, 'ASSISTANT', 1018, 201),
(60007, 231173, 200057, 'ASSISTANT', 1019, 202),
(60008, 231174, 200058, 'ASSISTANT', 1020, 203),
(60009, 231175, 200059, 'CHAIRMAN', 1001, 204),
(60010, 231176, 200060, 'CHAIRMAN', 1002, 205),
(60011, 231177, 200061, 'CHAIRMAN', 1003, 206);

-- --------------------------------------------------------

--
-- Table structure for table `program_enrolled`
--

CREATE TABLE IF NOT EXISTS `program_enrolled` (
`program_id` int(11) NOT NULL,
  `student_no` int(11) NOT NULL,
  `school_year` varchar(15) NOT NULL,
  `semester` varchar(15) NOT NULL,
  `course_code` int(11) NOT NULL,
  `year_lvl` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
`project_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `project_name` varchar(45) NOT NULL,
  `project_desc` text,
  `project_type` varchar(15) DEFAULT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'Pending'
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`project_id`, `account_id`, `project_name`, `project_desc`, `project_type`, `status`) VALUES
(2, 1, 'sds', 'dsfd', 'web', 'Active'),
(3, 1, 'sample', 'sdkfjds', 'web', 'Active'),
(4, 1, 'Sample II', '', 'web', 'Active'),
(5, 1, 'final_testing', 'final', 'web', 'Active'),
(6, 1, 'archie', 'weee', 'web', 'Active'),
(7, 1, 'try', 'dfsdgf', 'web', 'Active'),
(8, 1, 'sample_latest', 'dshfjsdhfsdh', 'web', 'Active'),
(9, 1, 'sadasd', 'sfdsdfgdhsdjbchdgf', 'web', 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `report_account`
--

CREATE TABLE IF NOT EXISTS `report_account` (
`report_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `account_num` int(11) NOT NULL,
  `username` int(11) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `project_id` int(11) NOT NULL,
  `access_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `student_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `account` double DEFAULT NULL,
  `course` int(11) DEFAULT NULL,
  `major` varchar(45) DEFAULT NULL,
  `year_lvl` int(11) DEFAULT NULL,
  `semester` varchar(10) DEFAULT NULL,
  `school_year` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`student_id`, `person_id`, `account`, `course`, `major`, `year_lvl`, `semester`, `school_year`) VALUES
(1000000001, 4, 0, 1, '', 1, '2', '2013-2014'),
(2001001001, 140, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001002, 141, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001003, 142, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001004, 143, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001005, 144, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001006, 145, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001007, 146, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001008, 147, 0, 1, 'WEB APPLICATION DEVELOPMENT', 3, '2', '2013-2014'),
(2001001009, 148, 0, 1, 'WEB APPLICATION DEVELOPMENT', 3, '2', '2013-2014'),
(2001001010, 149, 0, 1, 'WEB APPLICATION DEVELOPMENT', 3, '2', '2013-2014'),
(2001001011, 150, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001012, 151, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001013, 152, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001014, 153, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001015, 154, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001016, 155, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001017, 156, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001018, 157, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001019, 158, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001020, 159, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001021, 160, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001022, 161, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001023, 162, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001024, 163, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001025, 164, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001026, 165, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001027, 166, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001028, 167, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001029, 168, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001030, 169, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001031, 170, 0, 1, 'WEB APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001032, 171, 0, 1, 'MOBILE APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001033, 172, 0, 1, 'MOBILE APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001034, 173, 0, 1, 'MOBILE APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001035, 174, 0, 1, 'MOBILE APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001036, 175, 0, 1, 'MOBILE APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001037, 176, 0, 1, 'MOBILE APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001038, 177, 0, 1, 'MOBILE APPLICATION DEVELOPMENT', 4, '2', '2013-2014'),
(2001001039, 178, 0, 1, 'MULTI MEDIA', 4, '2', '2013-2014'),
(2001001040, 179, 0, 1, 'MULTI MEDIA', 4, '2', '2013-2014'),
(2001001041, 180, 0, 1, 'MULTI MEDIA', 4, '2', '2013-2014'),
(2001001042, 181, 0, 1, 'MULTI MEDIA', 4, '2', '2013-2014'),
(2006007993, 2, 0, 1, 'WEB APPLICATIONS DEVELOPMENT', 4, '2', '2013-2014'),
(2006009003, 1, 0, 1, 'WEB APPLICATIONS DEVELOPMENT', 4, '2', '2013-2014'),
(2010006085, 3, 0, 1, 'WEB APPLICATIONS DEVELOPMENT', 4, '2', '2013-2014');

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE IF NOT EXISTS `subject` (
  `subj_id` int(11) NOT NULL,
  `college_id` int(11) DEFAULT NULL,
  `subj_name` varchar(45) NOT NULL,
  `subj_desc` text,
  `units` int(11) NOT NULL DEFAULT '0',
  `lecture_hrs` int(11) NOT NULL DEFAULT '0',
  `lab_hrs` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`subj_id`, `college_id`, `subj_name`, `subj_desc`, `units`, `lecture_hrs`, `lab_hrs`) VALUES
(1000, 18, 'CWTS 11', 'CIVIC WELFARE TRAINING SERVICE 11', 3, 4, 0),
(1001, 16, 'ENGLISH 1', 'STUDY AND THINKING SKILLS', 3, 3, 0),
(1002, 18, 'GUIDANCE 1', 'ADJUSTMENT TO COLLEGE LIFE (PHASE 1)', 0, 1, 0),
(1003, 1, 'IT 110', 'COMPUTER CONCEPTS AND OPERATIONS', 3, 3, 2),
(1004, 1, 'IT 111', 'COMPUTER PROGRAMMING 1', 3, 3, 2),
(1005, 1, 'KB 1', 'BASIC KEYBOARDING', 3, 3, 2),
(1006, 16, 'MATH 1', 'COLLEGE ALGEBRA', 3, 3, 0),
(1007, 16, 'MATH 2', 'PLANE TRIGONOMETRY', 3, 3, 0),
(1008, 18, 'PE 1', 'PHYSICAL FITNESS', 3, 2, 0),
(1009, 18, 'REED 1', 'REVELATION AND FAITH', 3, 3, 0),
(2000, 18, 'CWTS 12', 'CIVIC WELFARE TRAINING SERVICE 12', 3, 4, 0),
(2001, 16, 'ENGLISH 2', 'WRITING IN THE WRITING DISCIPLINE', 3, 3, 0),
(2002, 18, 'GUIDANCE 2', 'ADJUSTMENT TO COLLEGE LIFE (PHASE 2)', 0, 1, 0),
(2003, 1, 'IT 120', 'APPLIED SOFTWARE', 3, 3, 1),
(2004, 1, 'IT 121', 'COMPUTER PROGRAMMING II', 3, 3, 2),
(2005, 1, 'IT 122', 'DISCRETE STRUCTURE I', 3, 3, 0),
(2006, 16, 'MATH 4', 'ANALYTIC GEOMETRY', 3, 3, 0),
(2007, 18, 'PE 2', 'RHYTHMIC ACTIVITIES', 2, 2, 0),
(2008, 16, 'PHILO 1', 'LOGIC AND CRITICAL THINKING', 3, 3, 0),
(2009, 18, 'REED 2', 'CHRISTOLOGY', 3, 3, 0),
(3000, 16, 'ENGLISH 3', 'SPEECH IMPROVEMENT AND ORAL COMMUNICATIONS', 3, 3, 0),
(3001, 16, 'ENGLISH 4', 'PHILIPPINE LITERATURE IN ENGLISH ', 3, 3, 0),
(3002, 1, 'IT 211', 'DATA STRUCTURES AND ALGORITHMS', 3, 3, 2),
(3003, 1, 'IT 212', 'COMPUTER SYSTEMS & PC TECHNOLOGY', 3, 3, 2),
(3004, 1, 'IT 213', 'OBJECT ORIENTED PROGRAMMING 1', 3, 3, 2),
(3005, 16, 'MATH 7', 'STATISTICS & PROBABILITIES', 3, 3, 0),
(3006, 18, 'PE 3', 'INDIVIDUAL/DUAL SPORTS/GAMES', 2, 2, 0),
(3007, 18, 'REED 3', 'CHRUCH AND SACRAMENTS', 3, 3, 0),
(3008, 16, 'CHEM1', 'GENERAL CHEMISTRY (LEC/LAB)', 3, 3, 0),
(3009, 16, 'ENGLISH 23', 'PUBLIC SPEAKING', 3, 3, 0),
(4000, 1, 'IT 221', 'OBJECT ORIENTED PROGRAMMING II', 3, 3, 2),
(4001, 1, 'IT 222', 'DATABASE SYSTEMS 1', 3, 3, 2),
(4002, 1, 'IT 223', 'FUNDAMENTALS OF DIGITAL LOGIC DESIGN', 3, 3, 0),
(4003, 18, 'PE 4', 'TEAM SPORTS/GAMES', 2, 2, 0),
(4004, 16, 'PSYCH 1', 'GEN PSYCHOLOGY INCLUDING DRUG ADDICTION PREVENTION', 3, 3, 0),
(4005, 18, 'REED 4', 'CHRISTIAN MORALITY', 3, 3, 0),
(4006, 1, 'IT 231', 'WE DEVELOPMENT', 3, 3, 2),
(4007, 1, 'IT 232', 'ETHIC & SOCIAL RESPONSIBILITY IN THE IT PROFESSION', 3, 3, 0),
(4008, 1, 'IT 233', 'MULTIMEDIA SYSTEMS', 3, 3, 0),
(5001, 1, 'IT 311', 'SYSTEM ANALYSIS & DESIGN', 0, 3, 2),
(5002, 1, 'IT 312', 'DATABASE SYSTEMS II', 0, 3, 2),
(5003, 1, 'IT 313 ', 'COMPUTER NETWORKS 1', 0, 3, 2),
(5004, 1, 'IT 314', 'RESEARCH METHODS IN IT/COMPUTER SCIENCE', 0, 3, 2),
(5005, 15, 'ACCTG A', 'PRINCIPLES OF ACCOUNTING/SINGLE PROPRIETORSHIP', 6, 3, 0),
(5006, 18, 'FILIPINO 1', 'KOMUNIKASYONG SA AKADEMIKONG PILIPINO', 3, 3, 0),
(5007, 1, 'IT ELECTIVE 1A', 'WEB APPLICATIONS DEVELOPMENT I', 3, 3, 2),
(6002, 15, 'ACCTG B', 'PARTNERSHIP & CORPORATION ACCOUNTING', 3, 3, 0),
(6003, 16, 'PHYSICS 1', 'COLLEGE PHYSICS 1 (LEC/LAB)', 3, 3, 0),
(6004, 18, 'FILIPINO 2', 'PAGBASA''T PAGSULAT SA IBA''T-IBANG DISIPLINA', 3, 3, 0),
(6005, 1, 'IT ELEC 2A', 'WEB APPLICATIONS DEVELOPMENT II', 3, 3, 2),
(6006, 1, 'IT 321', 'IT PRACTICUM I', 3, 4, 0),
(6007, 1, 'IT 323', 'PRESENTATION SKILLS', 3, 3, 0),
(6008, 1, 'IT 324', 'SOFTWARE ENGINEERING 3', 3, 3, 2),
(6009, 1, 'IT 322', 'COMPUTER NETWORK II', 3, 3, 2),
(7000, 1, 'IT 331', 'TECHNOPRENEURSHIP', 3, 3, 2),
(7001, 1, 'IT 332', 'APPLIED OPERATING SOFTWARE', 3, 3, 2),
(7002, 1, 'FE 1', 'FREE ELECTIVE 1', 3, 3, 2),
(7003, 16, 'ECON 1', 'PRINCIPLES OF ECONOMICS WITH TAXATION & LAND REFORM', 3, 3, 0),
(7004, 16, 'POL SCI 1', 'POLITICS AND GOVERNANCE', 3, 3, 0),
(7005, 16, 'SOCIO 1', 'SOCIETY AND CULTURE', 3, 3, 0),
(7006, 1, 'IT ELEC 3A', 'WEB APPLICATIONS III', 3, 3, 2),
(7007, 1, 'IT 414', 'IT PRACTICUM II', 3, 4, 0),
(7008, 1, 'IT ELEC 4', 'IT ELECTIVE 4', 3, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `subj_enrolled`
--

CREATE TABLE IF NOT EXISTS `subj_enrolled` (
  `school_year` varchar(15) NOT NULL,
  `semester` varchar(15) NOT NULL,
  `student_no` int(11) NOT NULL,
  `offer_code` int(11) NOT NULL,
  `subj_code` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subj_enrolled`
--

INSERT INTO `subj_enrolled` (`school_year`, `semester`, `student_no`, `offer_code`, `subj_code`) VALUES
('2010-2011', '1', 2006007993, 1122, 1002),
('2010-2011', '1', 2006007993, 1123, 1003),
('2010-2011', '1', 2006007993, 1124, 1004),
('2010-2011', '1', 2006007993, 1125, 1005),
('2010-2011', '1', 2006009003, 1111, 1001),
('2010-2011', '1', 2006009003, 1112, 1002),
('2010-2011', '1', 2006009003, 1113, 1003),
('2010-2011', '1', 2006009003, 1114, 1004),
('2010-2011', '1', 2006009003, 1115, 1005),
('2010-2011', '1', 2006009003, 1116, 1006),
('2010-2011', '1', 2010006085, 1117, 1007),
('2010-2011', '1', 2010006085, 1118, 1008),
('2010-2011', '1', 2010006085, 1119, 1009),
('2010-2011', '1', 2010006085, 1121, 1001),
('2010-2011', '2', 2006009003, 2223, 2003),
('2010-2011', '2', 2006009003, 2224, 2004),
('2010-2011', '2', 2006009003, 2225, 2005),
('2011-2012', '1', 2006007993, 3333, 3002),
('2011-2012', '1', 2006007993, 3334, 3003),
('2011-2012', '1', 2006007993, 3335, 3004),
('2012-2013', '2', 2006009003, 4441, 4000),
('2012-2013', '2', 2006009003, 4442, 4001),
('2012-2013', '2', 2006009003, 4443, 4002),
('2013-2014', '1', 2006009003, 1215, 6006),
('2013-2014', '1', 2006009003, 1216, 6007),
('2013-2014', '1', 2006009003, 1217, 6008),
('2013-2014', '1', 2006009003, 1218, 6009),
('2013-2014', '2', 2006009003, 1311, 7000),
('2013-2014', '2', 2006009003, 1312, 7001),
('2013-2014', '2', 2006009003, 1313, 7002);

-- --------------------------------------------------------

--
-- Table structure for table `subj_offering`
--

CREATE TABLE IF NOT EXISTS `subj_offering` (
  `offer_code` int(11) NOT NULL,
  `subj_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subj_offering`
--

INSERT INTO `subj_offering` (`offer_code`, `subj_id`) VALUES
(1000, 5002);

-- --------------------------------------------------------

--
-- Table structure for table `subj_schedule`
--

CREATE TABLE IF NOT EXISTS `subj_schedule` (
`subj_sched_id` int(11) NOT NULL,
  `offer_code` int(11) NOT NULL,
  `school_year` varchar(15) NOT NULL,
  `semester` varchar(15) NOT NULL,
  `day` varchar(45) NOT NULL,
  `time` varchar(45) NOT NULL,
  `duration` varchar(45) NOT NULL,
  `room_no` varchar(45) NOT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `subj_id` int(10) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subj_schedule`
--

INSERT INTO `subj_schedule` (`subj_sched_id`, `offer_code`, `school_year`, `semester`, `day`, `time`, `duration`, `room_no`, `teacher_id`, `subj_id`) VALUES
(112, 1000, '2014-2015', '2', 'TTH', '10:00 - 11:00', '', 'BCL9', 500000004, 5002),
(113, 1001, '2014-2015', '2', 'TTH', '08:30 - 10:00', '', 'BCL9', 500000003, 6005);

-- --------------------------------------------------------

--
-- Table structure for table `tables`
--

CREATE TABLE IF NOT EXISTS `tables` (
`table_id` int(11) NOT NULL,
  `table_dbname` varchar(45) NOT NULL,
  `table_name` varchar(45) NOT NULL,
  `table_desc` text,
  `func` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tables`
--

INSERT INTO `tables` (`table_id`, `table_dbname`, `table_name`, `table_desc`, `func`) VALUES
(1, 'college', 'College', 'It contains the College Name, Dean name and Location of the college', 1),
(2, 'courses', 'Courses', 'It contains the college where the course belong, the course abbreviation, the course name and course description.\r\n', 1),
(3, 'department', 'Department', 'It contains the department name, the department head and location of department\r\n', 1),
(4, 'subject', 'Subject', 'It contains the subject information (subject code, subject name, subject description, units, laboratory and lecture hours. It also contains the subject offering and schedule. ', 1),
(5, 'subj_offering', 'Subject Offering and Subject Schedule', 'It contains the offering of every subject and schedules of every offering', 0),
(6, 'subj_enrolled', 'Subject Enrolled by Students and Grades', 'It contains the subjects enrolled by the students', 0),
(7, 'student', 'Students', 'It contains the basic information of a student, the subjects enrolled and the grades of the subject enrolled. ', 1),
(8, 'teacher', 'Teacher', 'It contains the information of the teacher and subjects handled. ', 1),
(9, 'personnel', 'Personnel/Staff/Admin', 'It contains the basic information of a personnel\r\n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE IF NOT EXISTS `teacher` (
  `teacher_id` int(11) NOT NULL,
  `sss` int(11) NOT NULL,
  `tin` int(11) NOT NULL,
  `college_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`teacher_id`, `sss`, `tin`, `college_id`, `person_id`) VALUES
(500000001, 2000001, 1000001, 1, 182),
(500000002, 2000002, 1000002, 1, 183),
(500000003, 2000003, 1000003, 1, 184),
(500000004, 2000004, 1000004, 1, 185),
(500000005, 2000005, 1000005, 1, 186),
(500000006, 2000006, 1000006, 1, 187),
(500000007, 2000007, 1000007, 1, 188),
(500000008, 2000008, 1000008, 1, 189),
(500000009, 2000009, 1000009, 1, 190),
(500000010, 2000010, 1000010, 1, 191),
(500000011, 2000011, 1000011, 1, 192),
(500000012, 2000012, 1000012, 1, 193),
(500000013, 2000013, 1000013, 1, 194),
(500000014, 2000014, 1000014, 1, 195),
(500000015, 2000015, 1000015, 16, 208),
(500000016, 2000016, 1000016, 16, 209),
(500000017, 2000017, 1000017, 16, 210),
(500000018, 2000018, 1000018, 18, 211),
(500000019, 2000019, 1000019, 18, 212);

-- --------------------------------------------------------

--
-- Table structure for table `touch_faculty_report`
--

CREATE TABLE IF NOT EXISTS `touch_faculty_report` (
  `report_id` int(20) NOT NULL,
  `offer_code` int(20) NOT NULL,
  `date` varchar(20) NOT NULL,
  `first_checking` varchar(20) DEFAULT NULL,
  `last_checking` varchar(20) DEFAULT NULL,
  `checking_status` varchar(20) NOT NULL DEFAULT '0/2'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `touch_faculty_report`
--

INSERT INTO `touch_faculty_report` (`report_id`, `offer_code`, `date`, `first_checking`, `last_checking`, `checking_status`) VALUES
(40137333, 1214, '2015-01-20', 'ABSENT', 'ABSENT', '2/2');

-- --------------------------------------------------------

--
-- Table structure for table `touch_ims_accounts`
--

CREATE TABLE IF NOT EXISTS `touch_ims_accounts` (
  `person_id` int(5) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `touch_ims_accounts`
--

INSERT INTO `touch_ims_accounts` (`person_id`, `username`, `password`) VALUES
(500, 'Glendon', 'Pable');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_function`
--
ALTER TABLE `access_function`
 ADD PRIMARY KEY (`access_function_id`), ADD KEY `accessFunc_fk` (`access_id`), ADD KEY `funcAccess_fk` (`func_id`);

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
 ADD PRIMARY KEY (`account_no`,`username`), ADD KEY `personAccount_fk` (`person_id`);

--
-- Indexes for table `attempts`
--
ALTER TABLE `attempts`
 ADD PRIMARY KEY (`attempt_id`,`id_number`), ADD KEY `personAttempt_fk` (`person_id`);

--
-- Indexes for table `college`
--
ALTER TABLE `college`
 ADD PRIMARY KEY (`college_id`), ADD KEY `teacherCollege_fk` (`dean_name`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
 ADD PRIMARY KEY (`course_id`), ADD KEY `collegeCourse` (`college_id`);

--
-- Indexes for table `db_access`
--
ALTER TABLE `db_access`
 ADD PRIMARY KEY (`access_id`,`proj_id`), ADD KEY `projAccess_fk` (`proj_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
 ADD PRIMARY KEY (`dept_id`), ADD KEY `personnelDeptHead_fk` (`dept_head`);

--
-- Indexes for table `function`
--
ALTER TABLE `function`
 ADD PRIMARY KEY (`func_id`), ADD KEY `tableFunc_fk` (`table_id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
 ADD PRIMARY KEY (`student_no`,`subj_code`,`subj_name`), ADD KEY `studentGrades_fk` (`student_no`), ADD KEY `subjcodeGrades_fk` (`subj_code`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
 ADD PRIMARY KEY (`person_id`);

--
-- Indexes for table `personnel`
--
ALTER TABLE `personnel`
 ADD PRIMARY KEY (`personnel_id`), ADD KEY `personPersonnel_fk` (`person_id`), ADD KEY `deptPersonnel_fk` (`dept_id`), ADD KEY `personPeronnel_fk` (`person_id`);

--
-- Indexes for table `program_enrolled`
--
ALTER TABLE `program_enrolled`
 ADD PRIMARY KEY (`program_id`,`student_no`,`school_year`,`semester`), ADD KEY `programStudent` (`student_no`), ADD KEY `programCourse` (`course_code`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
 ADD PRIMARY KEY (`project_id`), ADD KEY `accountProject_fk` (`account_id`);

--
-- Indexes for table `report_account`
--
ALTER TABLE `report_account`
 ADD PRIMARY KEY (`report_id`), ADD KEY `accountRA_fk` (`account_num`), ADD KEY `accesscodeRA_fk` (`access_id`), ADD KEY `personRA_fk` (`person_id`), ADD KEY `projectRA_fk` (`project_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
 ADD PRIMARY KEY (`student_id`), ADD KEY `personStudent_fk` (`person_id`), ADD KEY `courseStudent_fk` (`course`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
 ADD PRIMARY KEY (`subj_id`), ADD KEY `deptSubj_fk` (`college_id`);

--
-- Indexes for table `subj_enrolled`
--
ALTER TABLE `subj_enrolled`
 ADD PRIMARY KEY (`school_year`,`semester`,`student_no`,`offer_code`,`subj_code`), ADD KEY `studentSE_fk` (`student_no`), ADD KEY `offercodeSE_fk` (`offer_code`), ADD KEY `subjSE_fk` (`subj_code`);

--
-- Indexes for table `subj_offering`
--
ALTER TABLE `subj_offering`
 ADD PRIMARY KEY (`offer_code`), ADD KEY `subjOffering_fk` (`subj_id`);

--
-- Indexes for table `subj_schedule`
--
ALTER TABLE `subj_schedule`
 ADD PRIMARY KEY (`subj_sched_id`,`offer_code`,`school_year`,`semester`,`day`,`time`), ADD KEY `teacherSched_fk` (`teacher_id`), ADD KEY `offerSched_fk` (`offer_code`);

--
-- Indexes for table `tables`
--
ALTER TABLE `tables`
 ADD PRIMARY KEY (`table_id`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
 ADD PRIMARY KEY (`teacher_id`), ADD KEY `personTeacher_fk` (`person_id`), ADD KEY `deptTeacher_fk` (`college_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_function`
--
ALTER TABLE `access_function`
MODIFY `access_function_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=322;
--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
MODIFY `account_no` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `attempts`
--
ALTER TABLE `attempts`
MODIFY `attempt_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `college`
--
ALTER TABLE `college`
MODIFY `college_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `db_access`
--
ALTER TABLE `db_access`
MODIFY `access_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `function`
--
ALTER TABLE `function`
MODIFY `func_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=218;
--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
MODIFY `person_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=501;
--
-- AUTO_INCREMENT for table `program_enrolled`
--
ALTER TABLE `program_enrolled`
MODIFY `program_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `report_account`
--
ALTER TABLE `report_account`
MODIFY `report_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subj_schedule`
--
ALTER TABLE `subj_schedule`
MODIFY `subj_sched_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=114;
--
-- AUTO_INCREMENT for table `tables`
--
ALTER TABLE `tables`
MODIFY `table_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
